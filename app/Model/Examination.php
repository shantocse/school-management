<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Examination extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'class_id', 'year_id'
    ];

    public function subjects() {
    	return $this->belongsToMany('App\Model\Subject');
    }
}
