<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Year;
use Session;

class Section extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'user_id'
    ];

    public function class() {
    	return $this->belongsTo('App\Model\ClassModel')->withTrashed();
    }

    public function teacher() {
    	return $this->belongsTo('App\Model\User', 'user_id', 'id')->withTrashed();
    }

    public function schedules() {
        $year_id = Session::get("current_year_id");
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year) {
                $year = Year::orderBy('created_at', 'desc')->first();
                if ($year)
                    $year_id = $year->id;
                else
                    $year_id = 0;
            } else {
                $year_id = $year->id;
            }
        }

        return $this->hasMany('App\Model\ClassSchedule', 'section_id', 'id')
                ->where('year_id', $year_id)
                ->with('teacher', 'subject')
                ->orderBy('start');
    }
}
