<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassSchedule extends Model
{
    use SoftDeletes;

	protected $table = 'class_schedules';
	protected $dates = ['deleted_at'];
    protected $fillable = [
        'class_id', 'section_id', 'day', 'teacher_id', 'subject_id', 'year_id', 'start', 'end'
    ];

    public function section() {
    	return $this->belongsTo('App\Model\Section');
    }

    public function class() {
    	return $this->belongsTo('App\Model\ClassModel', 'class_id', 'id');
    }

    public function subject() {
    	return $this->belongsTo('App\Model\Subject');
    }

    public function teacher() {
    	return $this->belongsTo('App\Model\User', 'teacher_id', 'id');
    }

    public function getStartAttribute($value) {
	    return date("h:i A", strtotime($value));
	}

	public function getEndAttribute($value) {
	    return date("h:i A", strtotime($value));
	}
}
