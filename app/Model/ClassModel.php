<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassModel extends Model
{
	use SoftDeletes;

	protected $table = 'classes';
	protected $dates = ['deleted_at'];
    protected $fillable = [
        'name'
    ];

    public function sections() {
    	return $this->hasMany('App\Model\Section', 'class_id', 'id')->with('teacher', 'schedules');
    }

    public function subjects() {
    	return $this->hasMany('App\Model\Subject', 'class_id', 'id')->with('teachers');
    }

    public function schedules() {
        return $this->hasMany('App\Model\ClassSchedule', 'class_id', 'id');
    }
}
