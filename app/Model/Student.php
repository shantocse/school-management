<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id', 'roll', 'class_id', 'section_id', 'year_id', 
        'guardian_id', 'guardian_relation'
    ];

    public function meta() {
    	return $this->belongsTo('App\Model\User', 'user_id', 'id')->withTrashed();
    }

    public function class() {
    	return $this->belongsTo('App\Model\ClassModel')->withTrashed();
    }

    public function section() {
    	return $this->belongsTo('App\Model\Section')->withTrashed();
    }

    public function year() {
    	return $this->belongsTo('App\Model\Year')->withTrashed();
    }
}
