<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'class_id'
    ];

    public function teachers() {
    	return $this->belongsToMany('App\Model\User');
    }

    public function examinations() {
    	return $this->belongsToMany('App\Model\Examination');
    }
}
