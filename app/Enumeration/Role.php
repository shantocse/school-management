<?php
	namespace App\Enumeration;

	class Role  {
		public static $ADMIN 		= 1;
		public static $TEACHER 		= 2;
		public static $STUDENT 		= 3;
		public static $GUARDIAN		= 4;
	}
?>