<?php
	namespace App\Enumeration;

	class Day  {
		public static $SUNDAY 		= 1;
		public static $MONDAY 		= 2;
		public static $TUESDAY 		= 3;
		public static $WEDNESDAY	= 4;
		public static $THURSDAY		= 5;
		public static $FRIDAY		= 6;
		public static $SATURDAY		= 7;
	}
?>