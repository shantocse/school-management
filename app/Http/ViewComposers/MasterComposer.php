<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Model\Year;

class MasterComposer
{
    private $current_year_id;


    public function __construct(Request $request)
    {
        $this->current_year_id = $request->session()->get('current_year_id');
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $years = Year::all();
        $current_year_name = null;

        foreach($years as $index => $year){
            if (!$this->current_year_id && $year->default == 1)
                $this->current_year_id = $year->id;

            if ($this->current_year_id == $year->id)
                $current_year_name = $year->name;

            if ($index == sizeof($years)-1 && !$current_year_name) {
                $this->current_year_id = $year->id;
                $current_year_name = $year->name;
            }
        }

        if (!$current_year_name)
            $this->current_year_id = null;

        $view->with([
                'current_year_id' => $this->current_year_id,
                'current_year_name' => $current_year_name,
                'years' => $years,
            ]);
    }
}