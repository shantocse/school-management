<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Year;

class YearController extends Controller
{
    public function index() {
    	$page_data = [
    			'page_title' => 'Academic Year',
    			'page_description' => 'Manage all academic years',
    			'menu_title' => 'Year'
    		];

		$years = Year::all();

    	return view('academic_year.index', compact('years'))->with($page_data);
    }

    public function addYear(Request $request) {
    	if ($request->default == 1)
    		Year::where('default', 1)->update(['default'=> 0]);

		$year = Year::create([
				'name' => $request->name,
				'default' => $request->default,
			]);
    }

    public function makeDefault(Request $request) {
    	Year::where('default', 1)->update(['default'=> 0]);
    	$year = Year::find($request->id);
    	$year->default = 1;
    	$year->save();
    }

    public function deleteYear(Request $request) {
    	$year = Year::find($request->id);

        $session_year_id = $request->session()->get('current_year_id');
        if ($session_year_id == $year->id)
            $request->session()->forget('current_year_id');
        
    	$year->delete();
    }

    public function editYear(Request $request) {
    	if ($request->default == 1)
    		Year::where('default', 1)->update(['default'=> 0]);

    	$year = Year::find($request->id);
    	$year->name = $request->name;
    	$year->default = $request->default;
    	$year->save();
    }
}
