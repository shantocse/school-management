<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassModel;
use App\Model\ClassSchedule;
use App\Model\Year;

class ClassScheduleController extends Controller
{
    public function index() {
    	$page_data = [
    			'page_title' => 'Class Schedule',
    			'page_description' => '',
    			'menu_title' => 'Class_Schedule'
    		];

		$classes = ClassModel::with('sections', 'subjects')->get();

    	return view('class_schedule.index', compact('classes'))->with($page_data);
    }

    public function add(Request $request) {
    	$year_id = $request->session()->get('current_year_id');
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year) {
                $year = Year::orderBy('created_at', 'desc')->first();
                if ($year)
                    $year_id = $year->id;
                else
                    $year_id = 0;
            } else {
                $year_id = $year->id;
            }
        }

        if ($year_id == 0) {
            return response()->json([
                'success' => false, 
                'message' => 'No Academic year found.'
            ]);
        }

        //Class overlap check for this day
        $schedule = ClassSchedule::where('start', '<', $request->end_time)
    						->where('end', '>', $request->start_time)
    						->where('class_id', $request->class_id)
    						->where('section_id', $request->section_id)
    						->where('day', $request->day)
    						->where('year_id', $year_id)
    						->with('subject')
    						->first();

		if ($schedule){
			return response()->json([
				'success' => false, 
				'message' => 'This timeline ('.$schedule->start.' to '.$schedule->end.') is already assigned for <strong>'.$schedule->subject->name.'</strong>.'
			]);
		}

		//Teacher overlap check
		$schedule = ClassSchedule::where('start', '<', $request->end_time)
    						->where('end', '>', $request->start_time)
    						->where('teacher_id', $request->teacher_id)
    						->where('day', $request->day)
    						->where('year_id', $year_id)
    						->with('teacher', 'class', 'section', 'subject')
    						->first();

		if ($schedule){
			return response()->json([
				'success' => false, 
				'message' => '<strong>'.$schedule->teacher->name.'</strong> has <strong>'.$schedule->subject->name.'</strong> class in <strong>'.$schedule->class->name.' - '.$schedule->section->name.'</strong> from '.$schedule->start.' to '.$schedule->end
			]);
		}

    	$schedule = ClassSchedule::create([
    			'class_id' => $request->class_id,
    			'section_id' => $request->section_id,
    			'day' => $request->day,
    			'subject_id' => $request->subject_id,
    			'teacher_id' => $request->teacher_id,
    			'year_id' => $year_id,
    			'start' => $request->start_time,
    			'end' => $request->end_time,
    		]);

    	return response()->json(['success' => true, 'message' => $schedule->id]);
    }

    public function edit(Request $request) {
        $year_id = $request->session()->get('current_year_id');
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year) {
                $year = Year::orderBy('created_at', 'desc')->first();
                if ($year)
                    $year_id = $year->id;
                else
                    $year_id = 0;
            } else {
                $year_id = $year->id;
            }
        }

        if ($year_id == 0) {
            return response()->json([
                'success' => false, 
                'message' => 'No Academic year found.'
            ]);
        }

        //Class overlap check for this day
        $schedule = ClassSchedule::where('start', '<', $request->end_time)
                            ->where('end', '>', $request->start_time)
                            ->where('class_id', $request->class_id)
                            ->where('section_id', $request->section_id)
                            ->where('day', $request->day)
                            ->where('year_id', $year_id)
                            ->where('id', '!=', $request->id)
                            ->with('subject')
                            ->first();

        if ($schedule){
            return response()->json([
                'success' => false, 
                'message' => 'This timeline ('.$schedule->start.' to '.$schedule->end.') is already assigned for <strong>'.$schedule->subject->name.'</strong>.'
            ]);
        }

        //Teacher overlap check
        $schedule = ClassSchedule::where('start', '<', $request->end_time)
                            ->where('end', '>', $request->start_time)
                            ->where('teacher_id', $request->teacher_id)
                            ->where('day', $request->day)
                            ->where('year_id', $year_id)
                            ->where('id', '!=', $request->id)
                            ->with('teacher', 'class', 'section', 'subject')
                            ->first();
                            
        if ($schedule){
            return response()->json([
                'success' => false, 
                'message' => '<strong>'.$schedule->teacher->name.'</strong> is already assigned for <strong>'.$schedule->subject->name.'</strong> class in <strong>'.$schedule->class->name.' - '.$schedule->section->name.'</strong> from '.$schedule->start.' to '.$schedule->end
            ]);
        }

        $schedule = ClassSchedule::where('id', $request->id)->first();
        $schedule->subject_id = $request->subject_id;
        $schedule->teacher_id = $request->teacher_id;
        $schedule->start = $request->start_time;
        $schedule->end = $request->end_time;
        $schedule->save();

        return response()->json(['success' => true, 'message' => 'Success']);
    }

    public function delete(Request $request) {
        $schedule = ClassSchedule::where('id', $request->id)->first();
        $schedule->delete();
    }
}
