<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Enumeration\Role;
use App\Model\Section;
use App\Model\ClassModel;
use App\Model\ClassSchedule;

class ClassController extends Controller
{
	public function index() {
		$page_data = [
    			'page_title' => 'Class',
    			'page_description' => 'Manage all classes',
    			'menu_title' => 'Class'
    		];

		$classes = ClassModel::with('sections')->get();

		return view('class.index', compact('classes'))->with($page_data);
	}

    public function showAddClass() {
    	$page_data = [
    			'page_title' => 'Add Class',
    			'page_description' => '',
    			'menu_title' => 'Class'
    		];

		$teachers = User::where('role', Role::$TEACHER)->get();

    	return view('class.add', compact('teachers'))->with($page_data);
    }

    public function postAddClass(Request $request) {
    	$class = ClassModel::create(['name' => $request->name]);

    	for($i=0; $i<sizeof($request->section_name); $i++) {
    		$sections[] = new Section([
    				'name' => $request->section_name[$i],
    				'user_id' => $request->teacher_id[$i]
    			]);
    	}

    	$class->sections()->saveMany($sections);

    	return redirect()->route('show_classes');
    }

    public function showEditClass($id) {
    	$page_data = [
    			'page_title' => 'Edit Class',
    			'page_description' => '',
    			'menu_title' => 'Class'
    		];

    	$class = ClassModel::where('id', $id)->with('sections')->first();
    	$teachers = User::where('role', Role::$TEACHER)->get();

    	return view('class.edit', compact('class', 'teachers'))->with($page_data);
    }

    public function postEditClass(ClassModel $class, Request $request) {
    	$class->name = $request->name;
    	$class->save();

        $section_ids = [];
        $new_ids = $request->previous_section_id;

        foreach($class->sections as $section){
            if (!in_array($section->id, $new_ids))
                $section_ids[] = $section->id;
        }

        if (!empty($section_ids)) {
            $sections = Section::where('class_id', $class->id)
                                ->whereIn('id', $section_ids)
                                ->delete();
            $schedules = ClassSchedule::whereIn('section_id', $section_ids)
                                ->delete();
        }

        if ($request->section_name) {
    	   for($i=0; $i<sizeof($request->section_name); $i++) {
        		$sections[] = new Section([
        				'name' => $request->section_name[$i],
        				'user_id' => $request->teacher_id[$i]
        			]);
        	}
            $class->sections()->saveMany($sections);
        }

    	return redirect()->route('show_classes');
    }

    public function deleteClass(Request $request) {
    	$class = ClassModel::where('id', $request->id)->first();

    	$class->sections()->delete();
        $class->schedules()->delete();
    	$class->delete();
    }
}
