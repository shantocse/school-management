<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassModel;
use App\Model\Examination;
use App\Model\Year;
use DateTime;

class ExaminationController extends Controller
{
    public function examinationList() {
    	$page_data = [
    			'page_title' => 'Examination List',
    			'page_description' => '',
    			'menu_title' => 'Examination List'
    		];

		$classes = ClassModel::with('subjects')->get();

		return view('examination.list', compact('classes'))->with($page_data);
    }

    public function showAddExamination() {
    	$page_data = [
    			'page_title' => 'Add Examination',
    			'page_description' => '',
    			'menu_title' => 'Examination List'
    		];

		$classes = ClassModel::with('subjects')->get();

    	return view('examination.add', compact('classes'))->with($page_data);
    }

    public function postAddExamination(Request $request) {
    	$message = [
    			'class_id.required' => 'Select a class.',
    			'subject_id.required' => 'Select a subject.',
                'marks.*.required' => 'Enter marks.',
                'marks.*.numeric' => 'The mark must be a number.',
                'dates.*.required' => 'Enter dates.',
    		];

    	$this->validate($request, [
    			'name' => 'required|max:100',
    			'class_id' => 'required',
    			'subject_id' => 'required',
    			'marks.*' => 'required|numeric',
                'dates.*' => 'required'
    		], $message);

        $year_id = $request->session()->get('current_year_id');
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year) {
                $year = Year::orderBy('created_at', 'desc')->first();
                if ($year)
                    $year_id = $year->id;
                else
                    $year_id = 0;
            } else {
                $year_id = $year->id;
            }
        }

        if ($year_id == 0) {
            dd('No Academic year found');
        }

        $subjects = [];
        for($i=0; $i<sizeof($request->subject_id); $i++) {
            $date = DateTime::createFromFormat('F d, Y - H:i A', $request->dates[$i]);
            $date = $date->format('Y-m-d H:i:s');

            $subjects[$request->subject_id[$i]] = [
                    'mark' => $request->marks[$i],
                    'date' => $date,
                ];
        }

        $examination = Examination::create([
                'name' => $request->name,
                'class_id' => $request->class_id,
                'year_id' => $year_id,
            ]);

        $examination->subjects()->attach($subjects);
    }
}
