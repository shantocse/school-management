<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassModel;
use App\Model\User;
use App\Model\Student;
use App\Model\Year;
use App\Enumeration\Role;
use Storage;
use Validator;

class StudentController extends Controller
{
    public function index(Request $request) {
    	$page_data = [
                'page_title' => 'Student',
                'page_description' => '',
                'menu_title' => 'Student'
            ];

        $year_id = $request->session()->get('current_year_id');
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year) {
                $year = Year::orderBy('created_at', 'desc')->first();
                if ($year)
                    $year_id = $year->id;
                else
                    $year_id = 0;
            } else {
                $year_id = $year->id;
            }
        }


        $students = Student::where('year_id', $year_id)
                ->with('meta', 'class', 'section')
                ->orderBy('created_at', 'desc')
                ->paginate(10);
    	return view('student.index', compact('students', 'year_id'))->with($page_data);
    }

    public function showAddStudent(Request $request) {
    	$page_data = [
                'page_title' => 'Add Student',
                'page_description' => '',
                'menu_title' => 'Student'
            ];

        $year_id = $request->session()->get('current_year_id');
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year) {
                $year = Year::orderBy('created_at', 'desc')->first();
                if ($year)
                    $year_id = $year->id;
                else
                    $year_id = 0;
            } else {
                $year_id = $year->id;
            }
        }

        $classes = ClassModel::with('sections')->get()->toArray();
        $guardians = User::where('role', Role::$GUARDIAN)->get();

     	return view('student.add', compact('classes', 'guardians', 'year_id'))->with($page_data);
    }

    public function postAddStudent(Request $request) {
        $rules = [
                'name' => 'required|max:100',
                'username' => 'required|max:20|unique:users',
                'roll' => 'required|unique:students,roll,NULL,
                            section_id,section_id,' .$request->section_id,
                'email' => 'sometimes|nullable|email|max:100|unique:users',
                'password' => 'required|min:6|confirmed',
                'gender' => 'required',
                'dob' => 'required|before:now',
                'class_id' => 'required',
                'section_id' => 'required',
                'relation' => 'required|max:100',
            ];

        $error_messages = [
            'gender.required' => 'Select a gender.',
            'dob.required' => 'Select date of birth.',
            'dob.before' => 'The date of birth must be a date before now.',
            'class_id.required' => 'Select a class.',
            'section_id.required' => 'Select a section.',
            'guardian_id.required' => 'Select a guardian.'
        ];

        if ($request->guardian_type == 'new') {
            $rules['guardian_name'] = 'required|max:100'; 
            $rules['guardian_username'] = 'required|max:100';
            $rules['guardian_email'] = 'sometimes|nullable|email|max:100|unique:users'; 
            $rules['guardian_password'] = 'required|min:6|confirmed'; 
            $rules['guardian_gender'] = 'required'; 
            $rules['guardian_phone'] = 'required';
        } else {
            $rules['guardian_id'] = 'required'; 
        }

        $this->validate($request, $rules, $error_messages);

        $data = $request->all();
        $meta = User::create([
                'name' => $data['name'],
                'username' => $data['username'],

                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'gender' => $data['gender'],
                'dob' => $data['dob'],
                'phone' => $data['phone-full'],
                'transportation_id' => $data['transportation_id'],
                'role' => Role::$STUDENT,
            ]);

        if ($request->guardian_type == 'new') {
            $guardian = User::create([
                'name' => $data['guardian_name'],
                'username' => $data['guardian_username'],
                'email' => $data['guardian_email'],
                'password' => bcrypt($data['guardian_password']),
                'gender' => $data['guardian_gender'],
                'phone' => $data['guardian_phone-full'],
                'transportation_id' => 0,
                'role' => Role::$GUARDIAN,
            ]);

            if ($request->guardian_photo_data) {
                $file = file_get_contents($request->guardian_photo_data);
                Storage::disk('profile_pic')->put($guardian->id.'.png', $file);
            }
            $guardian_id = $guardian->id;
        } else {
            $guardian_id = $data['guardian_id'];
        }

        $year_id = $request->session()->get('current_year_id');
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year)
                $year = Year::orderBy('created_at', 'desc')->first();

            $year_id = $year->id;
        }

        $student = Student::create([
                'user_id' => $meta->id,
                'roll' => $data['roll'],
                'class_id' => $data['class_id'],
                'section_id' => $data['section_id'],
                'year_id' => $year_id,
                'guardian_id' => $guardian_id,
                'guardian_relation' => $data['relation']
            ]);

        if ($request->photo_data) {
            $file = file_get_contents($request->photo_data);
            Storage::disk('profile_pic')->put($meta->id.'.png', $file);
        }

        return redirect()->route('show_students');
    }

    public function showEditStudent($id) {
        $page_data = [
                'page_title' => 'Edit Student',
                'page_description' => '',
                'menu_title' => 'Student'
            ];

        $student = Student::where('id', $id)->with('meta')->first();
        $classes = ClassModel::with('sections')->get()->toArray();
        $guardians = User::where('role', Role::$GUARDIAN)->get();

        return view('student.edit', compact('classes', 'guardians', 'student'))
                    ->with($page_data);
    }

    public function postEditStudent($id, Request $request) {
        $student = Student::where('id', $id)->first();
        $meta = $student->meta;

        $rules = [
                'name' => 'required|max:100',
                'username' => 'required|max:20|unique:users,username,'.$meta->id,
                'roll' => 'required|unique:students,roll,NULL,
                            section_id,section_id,'.$request->section_id.
                            ',id,id'.$student->id,
                'email' => 'sometimes|nullable|email|max:100|unique:users,email,'.$meta->id,
                'gender' => 'required',
                'dob' => 'required|before:now',
                'class_id' => 'required',
                'section_id' => 'required',
                'relation' => 'required|max:100',
            ];

        $error_messages = [
            'gender.required' => 'Select a gender.',
            'dob.required' => 'Select date of birth.',
            'dob.before' => 'The date of birth must be a date before now.',
            'class_id.required' => 'Select a class.',
            'section_id.required' => 'Select a section.',
            'guardian_id.required' => 'Select a guardian.'
        ];

        if ($request->guardian_type == 'new') {
            $rules['guardian_name'] = 'required|max:100'; 
            $rules['guardian_username'] = 'required|max:100';
            $rules['guardian_email'] = 'sometimes|nullable|email|max:100|unique:users'; 
            $rules['guardian_password'] = 'required|min:6|confirmed'; 
            $rules['guardian_gender'] = 'required'; 
            $rules['guardian_phone'] = 'required';
        } else {
            $rules['guardian_id'] = 'required'; 
        }

        $this->validate($request, $rules, $error_messages);

        $data = $request->all();

        $meta->name = $data['name'];
        $meta->username = $data['username'];
        $meta->email = $data['email'];
        $meta->gender = $data['gender'];
        $meta->dob = $data['dob'];
        $meta->phone = $data['phone-full'];
        $meta->transportation_id = $data['transportation_id'];

        if ($request->guardian_type == 'new') {
            $guardian = User::create([
                'name' => $data['guardian_name'],
                'username' => $data['guardian_username'],
                'email' => $data['guardian_email'],
                'password' => bcrypt($data['guardian_password']),
                'gender' => $data['guardian_gender'],
                'phone' => $data['guardian_phone-full'],
                'transportation_id' => 0,
                'role' => Role::$GUARDIAN,
            ]);

            if ($request->guardian_photo_data) {
                $file = file_get_contents($request->guardian_photo_data);
                Storage::disk('profile_pic')->put($guardian->id.'.png', $file);
            }
            $guardian_id = $guardian->id;
        } else {
            $guardian_id = $data['guardian_id'];
        }

        $student->roll = $data['roll'];
        $student->class_id = $data['class_id'];
        $student->section_id = $data['section_id'];
        $student->guardian_id = $guardian_id;
        $student->guardian_relation = $data['relation'];

        $student->save();
        $meta->save();

        if ($request->photo_data) {
            $file = file_get_contents($request->photo_data);
            Storage::disk('profile_pic')->put($meta->id.'.png', $file);
        } else {
            if ($request->photo_change)
                Storage::delete('profile_pic/'.$meta->id.'.png');
        }

        return redirect($data['redirect_url']); 
    }

    public function deleteStudent(Request $request) {
        $student = Student::where('id', $request->id)->first();
        $student->delete();
    }
}
