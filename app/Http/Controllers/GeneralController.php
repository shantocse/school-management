<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class GeneralController extends Controller
{
    public function getProfilePic($id) {
    	if (Storage::exists('profile_pic/'.$id.'.png'))
	    	return Storage::get('profile_pic/'.$id.'.png');
	    else
	    	return Storage::get('profile_pic/default-user.png');
    }

    public function changeYear(Request $request) {
    	$year_id = $request->year;
    	$request->session()->put('current_year_id', $year_id);
    	return back();
    }
}
