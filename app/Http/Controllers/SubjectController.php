<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Subject;
use App\Model\ClassModel;
use App\Enumeration\Role;

class SubjectController extends Controller
{
    public function index() {
    	$page_data = [
    			'page_title' => 'Subject',
    			'page_description' => 'Manage all subjects',
    			'menu_title' => 'Subject'
    		];

		$classes = ClassModel::with('subjects')->get();

    	return view('subject.index', compact('classes'))->with($page_data);
    }

    public function showAddSubject() {
    	$page_data = [
    			'page_title' => 'Add Subject',
    			'page_description' => '',
    			'menu_title' => 'Subject'
    		];

		$teachers = User::where('role', Role::$TEACHER)->get();
		$classes = ClassModel::all();

		return view('subject.add', compact('teachers', 'classes'))->with($page_data);
    }

    public function postAddSubject(Request $request) {
    	$error_messages = [
            'teacher_id.*.required' => 'Select a teacher.',
            'teacher_id.*.distinct' => 'Select a teacher once.',
            'class_id.required' => 'Select a class.',
            'name.unique' => 'Already added.'
        ];

    	$this->validate($request, [
    		'name' => 'required|max:100|unique:subjects,name,NULL,
                            class_id,class_id,' .$request->class_id,
    		'class_id' => 'required',
			'teacher_id.*' => 'required|distinct'
		], $error_messages);

		$subject = Subject::create([
				'name' => $request->name,
				'class_id' => $request->class_id,
			]);

		$subject->teachers()->attach($request->teacher_id);

		return redirect()->route('show_subjects');
    }

    public function showEditSubject(Request $request) {
        $page_data = [
                'page_title' => 'Edit Subject',
                'page_description' => '',
                'menu_title' => 'Subject'
            ];

        $subject = Subject::where('id', $request->id)->with('teachers')->first();
        $teachers = User::where('role', Role::$TEACHER)->get();
        $classes = ClassModel::all();

        return view('subject.edit', compact('classes', 'teachers', 'subject'))
                    ->with($page_data);
    }

    public function postEditSubject($id, Request $request) {
        $error_messages = [
            'teacher_id.*.required' => 'Select a teacher.',
            'teacher_id.*.distinct' => 'Select a teacher once.',
            'class_id.required' => 'Select a class.',
            'name.unique' => 'Already added.'
        ];

        $this->validate($request, [
            'name' => 'required|max:100|unique:subjects,name,NULL,
                            class_id,class_id,' .$request->class_id.
                            ',id,id'.$id,
            'class_id' => 'required',
            'teacher_id.*' => 'required|distinct'
        ], $error_messages);


        $subject = Subject::where('id', $id)->first();

        $subject->name = $request->name;
        $subject->class_id = $request->class_id;
        $subject->teachers()->sync($request->teacher_id);
        $subject->save();
 
        return redirect()->route('show_subjects'); 
    }

    public function deleteSubject(Request $request) {
        $subject = Subject::where('id', $request->id);
        $subject->delete();
    }
}
