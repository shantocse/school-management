<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Enumeration\Role;
use Storage;

class GuardianController extends Controller
{
    public function index() {
    	$page_data = [
    			'page_title' => 'Guardian',
    			'page_description' => 'Manage all guardians',
    			'menu_title' => 'Guardian'
    		];

        $guardians = User::where('role', Role::$GUARDIAN)->orderBy('name')->paginate(10);

    	return view('guardian.index', compact('guardians'))->with($page_data);
    }

    public function showAddGuardian() {
    	$page_data = [
    			'page_title' => 'Add Guardian',
    			'page_description' => '',
    			'menu_title' => 'Guardian'
    		];

    	return view('guardian.add')->with($page_data);
    }

    public function postAddGuardian(Request $request) {
    	$error_messages = [
	        'gender.required' => 'Select a gender.',
	    ];

    	$this->validate($request, [
    			'name' => 'required|max:100',
    			'username' => 'required|max:20|unique:users',
    			'email' => 'sometimes|nullable|email|max:100|unique:users',
    			'password' => 'required|min:6|confirmed',
    			'gender' => 'required',
    			'phone' => 'required'
    		], $error_messages);

    	$data = $request->all();
        $user = User::create([
                'name' => $data['name'],
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'gender' => $data['gender'],
                'phone' => $data['phone-full'],
                'role' => Role::$GUARDIAN
            ]);

        if ($request->photo_data) {
            $file = file_get_contents($request->photo_data);
            Storage::disk('profile_pic')->put($user->id.'.png', $file);
        }

        return redirect()->route('show_guardians');
    }

    public function showEditGuardian(User $guardian) {
    	$page_data = [
                'page_title' => 'Edit Guardian',
                'page_description' => '',
                'menu_title' => 'Guardian'
            ];

        return view('guardian.edit', compact('guardian'))->with($page_data);
    }

    public function postEditGuardian(Request $request, User $guardian){
    	$error_messages = [
            'gender.required' => 'Select a gender.',
        ];

        $this->validate($request, [
                'name' => 'required|max:100',
                'username' => 'required|max:20|unique:users,username,'.$guardian->id,
                'email' => 'sometimes|nullable|email|max:100|unique:users,email,'.$guardian->id,
                'gender' => 'required',
                'phone' => 'required'
            ], $error_messages);

        $data = $request->all();
        $guardian->name = $data['name'];
        $guardian->username = $data['username'];
        $guardian->email = $data['email'];
        $guardian->gender = $data['gender'];
        $guardian->phone = $data['phone-full'];
        $guardian->save();

        if ($request->photo_data) {
            $file = file_get_contents($request->photo_data);
            Storage::disk('profile_pic')->put($guardian->id.'.png', $file);
        } else {
            if ($request->photo_change)
                Storage::delete('profile_pic/'.$guardian->id.'.png');
        }

        return redirect($data['redirect_url']);
    }

    public function deleteGuardian(Request $request) {
        $guardian = User::find($request->id);
        $guardian->delete();
    }
}
