<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Enumeration\Role;
use Storage;

class TeacherController extends Controller
{
    public function index() {
    	$page_data = [
    			'page_title' => 'Teacher',
    			'page_description' => 'Manage all teachers',
    			'menu_title' => 'Teacher'
    		];

        $teachers = User::where('role', Role::$TEACHER)->orderBy('name')->paginate(10);

    	return view('teacher.index', compact('teachers'))->with($page_data);
    }

    public function showAddTeacher() {
    	$page_data = [
    			'page_title' => 'Add Teacher',
    			'page_description' => '',
    			'menu_title' => 'Teacher'
    		];

		return view('teacher.add')->with($page_data);
    }

    public function postAddTeacher(Request $request) {
    	$error_messages = [
	        'gender.required' => 'Select a gender.',
	        'dob.required' => 'Select date of birth.',
	        'dob.before' => 'The date of birth must be a date before now.'
	    ];

    	$this->validate($request, [
    			'name' => 'required|max:100',
    			'username' => 'required|max:20|unique:users',
    			'email' => 'required|email|max:100|unique:users',
    			'password' => 'required|min:6|confirmed',
    			'gender' => 'required',
    			'dob' => 'required|before:now',
    			'phone' => 'required'
    		], $error_messages);

        $data = $request->all();
        $user = User::create([
                'name' => $data['name'],
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'gender' => $data['gender'],
                'dob' => $data['dob'],
                'phone' => $data['phone-full'],
                'transportation_id' => $data['transportation'],
                'role' => Role::$TEACHER
            ]);

        if ($request->photo_data) {
            $file = file_get_contents($request->photo_data);
            Storage::disk('profile_pic')->put($user->id.'.png', $file);
        }

        return redirect()->route('show_teachers');
    }

    public function showEditTeacher(User $teacher) {
        $page_data = [
                'page_title' => 'Edit Teacher',
                'page_description' => '',
                'menu_title' => 'Teacher'
            ];

        return view('teacher.edit', compact('teacher'))->with($page_data);
    }

    public function postEditTeacher(Request $request, User $teacher) {
        $error_messages = [
            'gender.required' => 'Select a gender.',
            'dob.required' => 'Select date of birth.',
            'dob.before' => 'The date of birth must be a date before now.'
        ];

        $this->validate($request, [
                'name' => 'required|max:100',
                'username' => 'required|max:20|unique:users,username,'.$teacher->id,
                'email' => 'required|email|max:100|unique:users,email,'.$teacher->id,
                'gender' => 'required',
                'dob' => 'required|before:now',
                'phone' => 'required'
            ], $error_messages);

        $data = $request->all();
        $teacher->name = $data['name'];
        $teacher->username = $data['username'];
        $teacher->email = $data['email'];
        $teacher->gender = $data['gender'];
        $teacher->dob = $data['dob'];
        $teacher->phone = $data['phone-full'];
        $teacher->save();

        if ($request->photo_data) {
            $file = file_get_contents($request->photo_data);
            Storage::disk('profile_pic')->put($teacher->id.'.png', $file);
        } else {
            if ($request->photo_change)
                Storage::delete('profile_pic/'.$teacher->id.'.png');
        }

        return redirect($data['redirect_url']);
    }

    public function deleteTeacher(Request $request) {
        $teacher = User::find($request->id);
        $teacher->delete();
    }
}
