<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enumeration\Role;
use App\Model\Student;
use App\Model\User;
use App\Model\Year;

class DashboardController extends Controller
{
    public function index(Request $request) {
		$year_id = $request->session()->get('current_year_id');
        if (!$year_id){
            $year = Year::where('default', 1)->first();
            if (!$year) {
                $year = Year::orderBy('created_at', 'desc')->first();
                if ($year)
                    $year_id = $year->id;
                else
                    $year_id = 0;
            } else {
                $year_id = $year->id;
            }
        }

    	$student_count = Student::where('year_id', $year_id)->count();
    	$teacher_count = User::where('role', Role::$TEACHER)->count();

    	$data = [
    			'student_count' => $student_count,
    			'teacher_count' => $teacher_count,
    			'page_title' => 'Dashboard',
    			'page_description' => '',
    			'menu_title' => 'Dashboard',
    		];

		return view('dashboard.index')->with($data);
    }
}
