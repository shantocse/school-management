<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('show_dashboard');
});

Auth::routes();

//Route::get('/home', 'HomeController@index');

//General
Route::get('storage/profile_pic/{id}', 'GeneralController@getProfilePic')->name('profile_pic');
Route::post('change/year', 'GeneralController@changeYear')->name('change_year');

//Dashboard
Route::get('dashboard', 'DashboardController@index')->name('show_dashboard');

//Student
Route::get('/student', 'StudentController@index')->name('show_students');
Route::get('/student/add', 'StudentController@showAddStudent')->name('show_add_student');
Route::post('/student/add', 'StudentController@postAddStudent')->name('post_add_student');
Route::get('/student/edit/{id}', 'StudentController@showEditStudent')->name('show_edit_student');
Route::post('/student/edit/{id}', 'StudentController@postEditStudent')->name('post_edit_student');
Route::post('/student/delete', 'StudentController@deleteStudent')->name('delete_student');

//Teacher
Route::get('/teacher', 'TeacherController@index')->name('show_teachers');
Route::get('/teacher/add', 'TeacherController@showAddTeacher')->name('show_add_teacher');
Route::post('/teacher/add', 'TeacherController@postAddTeacher')->name('post_add_teacher');
Route::get('/teacher/edit/{teacher}', 'TeacherController@showEditTeacher')->name('show_edit_teacher');
Route::post('/teacher/edit/{teacher}', 'TeacherController@postEditTeacher')->name('post_edit_teacher');
Route::post('/teacher/delete', 'TeacherController@deleteTeacher')->name('delete_teacher');

//Class
Route::get('/class', 'ClassController@index')->name('show_classes');
Route::get('/class/add', 'ClassController@showAddClass')->name('show_add_class');
Route::post('/class/add', 'ClassController@postAddClass')->name('post_add_class');
Route::get('/class/edit/{id}', 'ClassController@showEditClass')->name('show_edit_class');
Route::post('/class/edit/{class}', 'ClassController@postEditClass')->name('post_edit_class');
Route::post('/class/delete', 'ClassController@deleteClass')->name('delete_class');

//Academic Years
Route::get('/year', 'YearController@index')->name('show_years');
Route::post('/year/add', 'YearController@addYear')->name('add_year');
Route::post('/year/edit', 'YearController@editYear')->name('edit_year');
Route::post('/year/default', 'YearController@makeDefault')->name('make_default_year');
Route::post('/year/delete', 'YearController@deleteYear')->name('delete_year');

//Guardian
Route::get('/guardian', 'GuardianController@index')->name('show_guardians');
Route::get('/guardian/add', 'GuardianController@showAddGuardian')->name('show_add_guardian');
Route::post('/guardian/add', 'GuardianController@postAddGuardian')->name('post_add_guardian');
Route::get('/guardian/edit/{guardian}', 'GuardianController@showEditGuardian')->name('show_edit_guardian');
Route::post('/guardian/edit/{guardian}', 'GuardianController@postEditGuardian')->name('post_edit_guardian');
Route::post('/guardian/delete', 'GuardianController@deleteGuardian')->name('delete_guardian');

//Subject
Route::get('/subject', 'SubjectController@index')->name('show_subjects');
Route::get('/subject/add', 'SubjectController@showAddSubject')->name('show_add_subject');
Route::post('/subject/add', 'SubjectController@postAddSubject')->name('post_add_subject');
Route::get('/subject/edit/{id}', 'SubjectController@showEditSubject')->name('show_edit_subject');
Route::post('/subject/edit/{id}', 'SubjectController@postEditSubject')->name('post_edit_subject');
Route::post('/subject/delete', 'SubjectController@deleteSubject')->name('delete_subject');

//Class Schedule
Route::get('/class/schedule', 'ClassScheduleController@index')->name('show_class_schedule');
Route::post('/class/schedule/add', 'ClassScheduleController@add')->name('add_class_schedule');
Route::post('/class/schedule/edit', 'ClassScheduleController@edit')->name('edit_class_schedule');
Route::post('/class/schedule/delete', 'ClassScheduleController@delete')->name('delete_class_schedule');

//Examination
Route::get('/examination/list', 'ExaminationController@examinationList')->name('show_examinations_list');
Route::get('/examination/add', 'ExaminationController@showAddExamination')->name('show_add_examination');
Route::post('/examination/add', 'ExaminationController@postAddExamination')->name('post_add_examination');