@extends('layouts.app')

@section('content')
<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    	<div class="info-box-3 bg-pink hover-zoom-effect">
            <div class="icon">
                <i class="material-icons">people</i>
            </div>
            <div class="content">
                <div class="text">TOTAL STUDENT</div>
                <div class="number count-to" data-from="0" data-to="{{ $student_count }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box-3 bg-blue hover-zoom-effect">
            <div class="icon">
                <i class="material-icons">business_center</i>
            </div>
            <div class="content">
                <div class="text">TOTAL TEACHER</div>
                <div class="number count-to" data-from="0" data-to="{{ $teacher_count }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript">
	$(function() {
		$('.count-to').countTo();
	})
</script>
@stop