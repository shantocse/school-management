@extends('layouts.app')

@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
		    <div class="body bg-white clearfix">
	    		<label>Class</label>
	    		<select>
	    			<option selected>All Classes</option>
	    		</select>

	    		<label>Section</label>
	    		<select>
	    			<option selected>All Sections</option>
	    		</select>

				<div class="pull-right">
					<a href="{{ route('show_add_teacher') }}" class="btn btn-primary btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Add Teacher">
			            <i class="material-icons">add</i>
			    	</a>

			    	<a href="#" class="btn btn-success btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as PDF">
			            <i class="material-icons">picture_as_pdf</i>
			        </a>

			        <a href="#" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as CSV">
			            <i class="material-icons">insert_drive_file</i>
			        </a>

					<a href="#" class="btn btn-danger btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Print">
			            <i class="material-icons">print</i>
			        </a>
				</div>
		    </div>
		</div>
	</div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    LIST OF TEACHERS
                </h2>
            </div>

            <div class="body table-responsive">
                <table class="table table-hover" id="table-teachers">
                    <thead>
                        <tr>
                            <th></th>
                            <th>NAME</th>
                            <th>EMAIL</th>
                            <th>PHONE</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($teachers as $teacher)
	                        <tr>
	                        	<td>
	                        		<img class="img-circle img-responsive" src="{{ route('profile_pic', ['id' => $teacher->id]) }}" width="50xp">
	                        	</td>
	                            <td><a href="#">{{ $teacher->name }}</a></td>
	                            <td>{{ $teacher->email }}</td>
	                            <td>{{ $teacher->phone }}</td>
	                            <td class="text-right">
	                            	<div class="btn-group">
	                                    <button type="button" class="btn bg-cyan dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	                                         <i class="material-icons">more_vert</i>
	                                    </button>
	                                    <ul class="dropdown-menu dropdown-menu-right">
	                                        <li><a href="#" class=" waves-effect waves-block"><i class="material-icons">sms</i> Send Message</a></li>

	                                        <li><a href="#" class=" waves-effect waves-block"><i class="material-icons">smartphone</i> Send SMS</a></li>

	                                        <li><a href="#" class=" waves-effect waves-block"><i class="material-icons">email</i> Send Email</a></li>

	                                        <li role="separator" class="divider"></li>

	                                        <li><a href="{{ route('show_edit_teacher', ['teacher' => $teacher->id]) }}" class=" waves-effect waves-block"><i class="material-icons">edit</i> Edit</a></li>

	                                        <li><a class=" waves-effect waves-block delete" data-id="{{ $teacher->id }}"><i class="material-icons">delete</i> Delete</a></li>
	                                    </ul>
	                                </div>
	                            </td>
	                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $teachers->links() }}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-red">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Delete Teacher</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <button id="btn-delete" type="button" class="btn btn-link waves-effect">DELETE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript">
	$(function() {
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		$('.delete').click(function() {
			var id = $(this).data('id');
			var index = $(this).closest('tr').index('tr');
			$('#btn-delete').attr("data-id", id);
			$('#btn-delete').attr("data-index", index);
			$('#delete-modal').modal('show');
		});

		$('#btn-delete').click(function() {
			var id = $(this).data('id');
			var index = $(this).data('index');

			$.ajax({
				method: "POST",
				url: "{{ route('delete_teacher') }}",
				data: { id: id}
			}).done(function( msg ) {
				$('#table-teachers tr:eq('+index+')').remove();
				$('#delete-modal').modal('hide');
			});
		});
	});
</script>
@stop