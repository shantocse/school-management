@extends('layouts.app')

@section('additionalCSS')
<link rel="stylesheet" type="text/css" href="{{ url('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('plugins/intl-tel-input/css/intlTelInput.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('plugins/cropperjs/cropper.min.css') }}">
@stop

@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
	            <h2>
	                Teacher Information
	            </h2>
	        </div>

	        <div class="body">
	        	<form id="form-teacher" class="form-horizontal" action="{{ route('post_edit_teacher', ['teacher' => $teacher->id]) }}" method="POST">
	        		{{ csrf_field() }}
	        		<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="name">Name</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('name') ? ' focused error' : '' }}">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ old('name') ? old('name') : $teacher->name }}">
                                </div>
                                @if ($errors->has('name'))
			                        <label class="error">{{ $errors->first('name') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="username">Username</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('username') ? ' focused error' : '' }}">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Enter username" value="{{ old('username') ? old('username') : $teacher->username }}">
                                </div>
                                @if ($errors->has('username'))
			                        <label class="error">{{ $errors->first('username') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('email') ? ' focused error' : '' }}">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ old('email') ? old('email') : $teacher->email }}">
                                </div>
                                @if ($errors->has('email'))
			                        <label class="error">{{ $errors->first('email') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="gender">Gender</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <input type="radio" class="with-gap radio-col-blue" name="gender" id="gender_male" value="male" {{ $teacher->gender == 'male' ? 'checked' : '' }}>
                        		<label for="gender_male">Male</label>

                        		<input type="radio" class="with-gap radio-col-blue" name="gender" id="gender_female" value="female" {{ $teacher->gender == 'female' ? 'checked' : '' }}>
                        		<label for="gender_female">Female</label>
								
								@if ($errors->has('gender'))
			                        <label class="error">{{ $errors->first('gender') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="dob">Birthday</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('dob') ? ' focused error' : '' }}">
                                	<input type="text" name="dob" data-provide="datepicker" data-date-format="dd-mm-yyyy" class="datepicker form-control" id="dob" placeholder="Please choose a date" value="{{ old('dob') ? old('dob') : $teacher->dob }}">
                                </div>
                                @if ($errors->has('dob'))
			                        <label class="error">{{ $errors->first('dob') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="phone">Phone</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div id="form-line-phone" class="form-line{{ $errors->has('phone') ? ' focused error' : '' }}">
                                	<input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone-full') ? old('phone-full') : $teacher->phone }}">
									<input id="hidden" type="hidden" name="phone-full">
                                </div>
                                <label id="phone-error" class="error">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="dob">Transportation</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <select class="form-control" id="transportation" name="transportation">
									<option value="0">No Transportation</option>
								</select>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="dob">Photo</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <img id="photo_preview" class="img-responsive img-circle" src="{{ old('photo_data') ? old('photo_data') : route('profile_pic', ['id' => $teacher->id]) }}" width="50px">
							<input class="hide" type="file" accept="image/*" name="photo" id="photo">
							<input type="hidden" name="photo_data" id="photo_data" value="{{ old('photo_data') }}">
							<br>

							<a id="change_photo" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Select photo">
                                <i class="material-icons">image</i>
                            </a>

                            <a id="reset_photo" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Reset">
                                <i class="material-icons">refresh</i>
                            </a>
                        </div>
                    </div>

					<input type="hidden" name="redirect_url" value="{{ URL::previous() }}">
					<input type="hidden" name="photo_change" id="photo_change">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-4 col-sm-6 col-xs-7">
                            <button type="submit" class="btn btn-primary waves-effect">SAVE</button>
                            <a href="{{ URL::previous() }}" class="btn btn-default waves-effect">CANCEL</a>
                        </div>
                    </div>
	        	</form>
	        </div>
		</div>
	</div>
</div>


<div class="modal fade" id="crop_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Crop the image</h4>
            </div>
            <div class="modal-body">
                <div>
					<img id="image_modal" src="{{ url('img/photo3.jpg') }}" alt="Picture" width="400px" height="400px">
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="crop_photo" class="btn btn-link waves-effect">SELECT</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript" src="{{ url('plugins/cropperjs/cropper.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/momentjs/moment.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/intl-tel-input/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/intl-tel-input/js/utils.js') }}"></script>
<script>
$(function() {
	$("#phone").intlTelInput({
		preferredCountries: ['bd'],
		separateDialCode: true
    });

    $('#phone').focus(function() {
    	$(this).closest('.form-line').addClass('focused');
    })

	$('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        maxDate : new Date(),
        clearButton: true,
        weekStart: 1,
        time: false
    });

    var cropper = $('#image_modal');
	var cropBoxData;
	var canvasData;
	$("#form-teacher").submit(function() {
		$("#hidden").val($("#phone").intlTelInput("getNumber"));

		if ($("#phone").val() != '' && !$("#phone").intlTelInput("isValidNumber")){
			$('#form-line-phone').addClass('focused error');
			$('#phone-error').html('Invalid phone number.');
			return false;
		}
	});

	$('#crop_modal').on('shown.bs.modal', function () {
		cropper.cropper({
			autoCropArea: 0.5,
			aspectRatio: 1 / 1,
			dashed: false,
			viewMode: 1,
			dragMode: 'move',
			ready: function () {
				cropper.cropper('setCanvasData', canvasData);
				cropper.cropper('setCropBoxData', cropBoxData);
			}
		});
	}).on('hidden.bs.modal', function () {
		cropBoxData = cropper.cropper('getCropBoxData');
		canvasData = cropper.cropper('getCanvasData');
		cropper.cropper('destroy');
	});

	$('#change_photo').click(function() {
		$('#photo').click();
	});

	$( "#photo" ).change(function() {
		var _URL = window.URL || window.webkitURL;
		img = new Image();
		img.onerror = function() { 
			alert('Please chose an image file!'); 
		};

		img.onload = function () {
			$('#image_modal').attr('src',this.src);
			$('#crop_modal').modal('show');
		};
		img.src = _URL.createObjectURL(this.files[0]);
	});

	$('#crop_photo').click(function() {
		var cropped_image_data = cropper.cropper('getCroppedCanvas').toDataURL();
		$("#photo_preview").attr('src', cropped_image_data);
		$('#crop_modal').modal('hide');
		$('#photo').val('');
		$('#photo_data').val(cropped_image_data);
	});

	$('#reset_photo').click(function() {
		$("#photo_preview").attr("src", "{{ url('img/default-user.png') }}");
		$('#photo_data').val('');
        $('#photo_change').val('1');
	});
});
</script>
@stop