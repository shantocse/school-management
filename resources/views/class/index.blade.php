@extends('layouts.app')

@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
		    <div class="body bg-white clearfix">
				<div class="pull-right">
					<a href="{{ route('show_add_class') }}" class="btn btn-primary btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Add Class">
			            <i class="material-icons">add</i>
			    	</a>

			    	<a href="#" class="btn btn-success btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as PDF">
			            <i class="material-icons">picture_as_pdf</i>
			        </a>

			        <a href="#" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as CSV">
			            <i class="material-icons">insert_drive_file</i>
			        </a>

					<a href="#" class="btn btn-danger btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Print">
			            <i class="material-icons">print</i>
			        </a>
				</div>
		    </div>
		</div>
	</div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    LIST OF CLASSES
                </h2>
            </div>

            <div class="body table-responsive">
				<table class="table table-bordered" id="table-classes">
					<thead>
						<tr>
							<th>Name</th>
							<th>Section Name</th>
							<th>Teacher</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						@foreach($classes as $class)
							@foreach($class->sections as $section)
								@if ($loop->first)
									<tr>
										<td rowspan="{{ sizeof($class->sections) }}">{{ $class->name }}</td>
										<td>{{ $section->name }}</td>
										<td>{{ $section->teacher->name }}</td>
										<td class="text-right" rowspan="{{ sizeof($class->sections) }}">
											<div class="btn-group">
			                                    <button type="button" class="btn bg-cyan dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			                                         <i class="material-icons">more_vert</i>
			                                    </button>
			                                    <ul class="dropdown-menu dropdown-menu-right">
			                                        <li><a href="#" class=" waves-effect waves-block"><i class="material-icons">people</i> Students</a></li>

			                                        <li role="separator" class="divider"></li>

			                                        <li><a href="{{ route('show_edit_class', ['id' => $class->id]) }}" class=" waves-effect waves-block"><i class="material-icons">edit</i> Edit</a></li>

			                                        <li><a class=" waves-effect waves-block delete" data-id="{{ $class->id }}" data-row-count="{{ sizeof($class->sections) }}"><i class="material-icons">delete</i> Delete</a></li>
			                                    </ul>
			                                </div>
										</td>
									</tr>
								@else
									<tr>
										<td>{{ $section->name }}</td>
										<td>{{ $section->teacher->name }}</td>
									</tr>
								@endif
							@endforeach
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-red">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Delete Class</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <button id="btn-delete" type="button" class="btn btn-link waves-effect">DELETE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript">
	$(function() {
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		$('.delete').click(function() {
			var id = $(this).data('id');
			var row_count = $(this).data('row-count');
			var index = $(this).closest('tr').index('tr');
			$('#btn-delete').attr("data-id", id);
			$('#btn-delete').attr("data-index", index);
			$('#btn-delete').attr("data-row-count", row_count);
			$('#delete-modal').modal('show');
		});

		$('#btn-delete').click(function() {
			var id = $(this).data('id');
			var index = $(this).data('index');
			var row_count = $(this).data('row-count');

			$.ajax({
				method: "POST",
				url: "{{ route('delete_class') }}",
				data: { id: id}
			}).done(function( msg ) {
				for(i=1; i<row_count; i++)
					$('#table-classes tr:eq('+index+')').next().remove();

				$('#table-classes tr:eq('+index+')').remove();
				$('#delete-modal').modal('hide');
			});
		});
	});
</script>
@stop