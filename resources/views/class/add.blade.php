@extends('layouts.app')

@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
	            <h2>
	                Class Information
	            </h2>
	        </div>

	        <div class="body">
				<form class="form-horizontal" action="{{ route('post_add_class') }}" method="POST" id="class-form">
					{{ csrf_field() }}
					<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="name">Name</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line" id="form-line-name">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter class name">
                                </div>
		                        <label class="error" id="error-name"></label>
                            </div>
                        </div>
                    </div>

					<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="sections">Section(s)</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Name</th>
										<th>Teacher</th>
										<th></th>
									</tr>
								</thead>

								<tbody id="sections">
									
								</tbody>
							</table>

							<a id="btn-add-section" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Add Section"><i class="material-icons">add</i></a>
						</div>
					</div>

					<div class="row clearfix">
                        <div class="col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-4 col-sm-6 col-xs-7">
                            <button type="submit" class="btn btn-primary waves-effect">ADD CLASS</button>
                            <a href="{{ URL::previous() }}" class="btn btn-default waves-effect">CANCEL</a>
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>

<template id="section-item">
	<tr>
		<td class="section-name"></td>
		<td class="teacher-name"></td>
		<td class="text-right">
			<a role="button" class="btn-remove-section">
				<i class="material-icons">delete</i>
			</a>
		</td>

		<input type="hidden" class="input-section-name" name="section_name[]">
		<input type="hidden" class="input-teacher-id" name="teacher_id[]">
	</tr>
</template>

<div class="modal fade" role="dialog" id="add-section-modal">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Add Section</h4>
			</div>
			<div class="modal-body">
				<form>
					<label for="section-name" class="control-label">Section Name:</label>

					<div class="form-group">
                        <div class="form-line" id="form-line-section-name">
                            <input type="text" class="form-control" id="section-name" placeholder="Enter class name">
                        </div>
                        <label class="error" id="section-name-error"></label>
                    </div>

					<div class="form-group">
						<label for="message-text" class="control-label">Teacher:</label>
						<select class="form-control show-tick" id="select-teacher" data-live-search="true" title="Choose teacher">
							@foreach($teachers as $teacher)
								<option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
							@endforeach
						</select>
						<label class="error" id="section-teacher-error"></label>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btn-add-modal" class="btn btn-link waves-effect">ADD</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="alert-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-red">
            <div class="modal-header">
                <h4 class="modal-title">{{ config('app.name', 'Laravel') }}</h4>
            </div>
            <div class="modal-body">
                <p id="alert-message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script>
	$(function() {
		var teachers = <?php echo json_encode($teachers); ?>;

		$('#btn-add-section').click(function() {
			$('#add-section-modal').modal('show');
		});

		$('#btn-add-modal').click(function() {
			$('#form-line-section-name').removeClass('focused');
			$('#form-line-section-name').removeClass('error');
			$('#section-name-error').html('')
			$('#section-teacher-error').html('');

			var section_name = $('#section-name').val();
			var teacher_id = $('#select-teacher').val();
			var teacher_name = $("#select-teacher option:selected").text();

			if (!section_name){
				$('#form-line-section-name').addClass('focused error');
				$('#section-name-error').html('Enter section name.');
			}
			else if (!teacher_id){
				$('#section-teacher-error').html('Select a teacher.');
			}
			else {
				var html = $('#section-item').html();
				var row = $(html);
				$('#sections').append(row);
				$('.section-name:last').html(section_name);
				$('.teacher-name:last').html(teacher_name);
				$('.input-section-name:last').val(section_name);
				$('.input-teacher-id:last').val(teacher_id);

				$('#add-section-modal').modal('hide');
			}
		});

		$('#add-section-modal').on('show.bs.modal', function (e) {
			$('#form-line-section-name').removeClass('focused');
			$('#form-line-section-name').removeClass('error');
			$('#section-name-error').html('')
			$('#section-teacher-error').html('');
			$('#section-name').val('');
			$('#select-teacher').val('');
			$('#select-teacher').selectpicker('render');
		});

		$('body').on('click','.btn-remove-section',function(){
		     $(this).closest('tr').remove(); 
		});

		$("#class-form").submit(function() {
			$('#form-line-name').removeClass('error');
			$('#form-line-name').removeClass('focused');
			$('#error-name').html('');

			if (!$("#name").val()){
				$('#form-line-name').addClass('focused error');
				$('#form-line-name').addClass('');
				$('#error-name').html('Enter class name.');
				return false;
			} else if ($('.input-section-name').length == 0) {
				$('#alert-message').html('Add a section');
				$('#alert-modal').modal('show');
				return false;
			}
		});
	});
</script>
@stop