@extends('layouts.app')

@section('content')
@if (sizeof($classes) == 0)
<div class="alert alert-danger">
    <strong>Sorry!</strong> No class found. <a href="{{ route('show_add_class') }}" class="alert-link">Click here</a> to add class. 
</div>
@else
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
		    <div class="body bg-white clearfix">
				<div class="pull-right">
					<a href="{{ route('show_add_examination') }}" class="btn btn-primary btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Add Examination">
			            <i class="material-icons">add</i>
			    	</a>

			    	<a href="#" class="btn btn-success btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as PDF">
			            <i class="material-icons">picture_as_pdf</i>
			        </a>

			        <a href="#" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as CSV">
			            <i class="material-icons">insert_drive_file</i>
			        </a>

					<a href="#" class="btn btn-danger btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Print">
			            <i class="material-icons">print</i>
			        </a>
				</div>
		    </div>
		</div>
	</div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
            	<ul class="nav nav-tabs tab-nav-right" role="tablist">
            		@foreach($classes as $class)
                    	<li class="{{ $loop->first ? 'active' : '' }}" role="presentation"><a href="#{{ $class->id }}" data-toggle="tab" aria-expanded="false">{{ $class->name }}</a></li>
                    @endforeach
                </ul>

                <div class="tab-content">
                	@foreach($classes as $class)
	                    <div role="tabpanel" class="tab-pane fade {{ $loop->first ? 'in active' : '' }}" id="{{ $class->id }}">
	                    	<table class="table table-bordered">
	                    		<thead>
	                    			<tr>
	                    				<th>Examination Name</th>
	                    				<th>Subject</th>
	                    				<th>Mark</th>
	                    				<th>Date</th>
	                    			</tr>
	                    		</thead>

	                    		<tbody>
	                    			<tr>
	                    				<td rowspan="2">1st Term Exam</td>
	                    				<td>Bangla</td>
	                    				<td>100</td>
	                    				<td>14 May, 2017</td>
	                    			</tr>

	                    			<tr>
	                    				<td>English</td>
	                    				<td>100</td>
	                    				<td>14 May, 2017</td>
	                    			</tr>
	                    		</tbody>
	                    	</table>
	                    </div>
                    @endforeach
           		</div>
       		</div>
        </div>
    </div>
</div>
@endif
@stop