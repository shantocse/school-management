@extends('layouts.app')

@section('additionalCSS')
<link rel="stylesheet" type="text/css" href="{{ url('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
@stop

@section('content')

<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
	            <h2>
	                Examination Information
	            </h2>
	        </div>

	        <div class="body">
				<form class="form-horizontal" action="{{ route('post_add_examination') }}" method="POST" id="examination-form">
					{{ csrf_field() }}
					<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="name">Examination Name</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line {{ $errors->has('name') ? ' focused error' : '' }}" id="form-line-name">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter examination name" value="{{ old('name') }}">
                                </div>
		                        @if ($errors->has('name'))
			                        <label class="error">{{ $errors->first('name') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

					<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="sections">Class</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
							<div class="form-group">
                                <select class="form-control show-tick" id="class_id" name="class_id" title="Choose class">
									@foreach($classes as $class)
										<option value="{{ $class->id }}" data-class-index="{{ $loop->index }}" {{ old('class_id') == $class->id ? 'selected' : '' }}>{{ $class->name }}</option>
									@endforeach
								</select>

								@if ($errors->has('class_id'))
			                        <label class="error">{{ $errors->first('class_id') }}</label>
			                    @endif
                            </div>
						</div>
					</div>

					<div class="row clearfix">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-xs-12">
                           	<table class="table">
                           		<tbody id="content-subjects">
                           			@if(old('class_id'))
                           				<?php
                           					foreach($classes as $class) {
                           						if (old('class_id') == $class->id){
                           							$subjects = $class->subjects;
                           							break;
                           						}
                           					}

                           					$counter = 0;
                           					if (old('subject_id'))
	                           					$subject_ids = old('subject_id');
	                           				else
	                           					$subject_ids = [];
                           				?>
	                           			@foreach($subjects as $subject)
	                           				<tr>
												<td>
													<input id="{{ $subject->id }}" name="subject_id[]" type="checkbox" class="checkbox-subject filled-in chk-col-blue" value="{{ $subject->id }}" {{ in_array($subject->id, $subject_ids) ? 'checked' : '' }}>
													<label for="{{ $subject->id }}" class="checkbox-subject-label">{{ $subject->name }}</label>
												</td>

												<td>
													<?php
														$old_mark = (in_array($subject->id, $subject_ids) ? old('marks.'.$counter) : '');
													?>
													<div class="form-group form-float form-group-sm">
										                <div class="form-line {{ (!empty($old_mark)) ? 'focused' : '' }}">
										                    <input type="text" name="marks[]" class="input-mark form-control" value="{{ in_array($subject->id, $subject_ids) ? old('marks.'.$counter) : '' }}" {{ in_array($subject->id, $subject_ids) ? '' : 'disabled' }}>
										                    <label class="form-label">Mark</label>
										                </div>
										                @if (in_array($subject->id, $subject_ids))
											                @if ($errors->has('marks.'.$counter))
										                        <label class="error">{{ $errors->first('marks.'.$counter) }}</label>
										                    @endif
									                    @endif
										            </div>
												</td>

												<td>
													<?php
														$old_date = (in_array($subject->id, $subject_ids) ? old('dates.'.$counter) : '');
													?>
													<div class="form-group form-float form-group-sm">
										                <div class="form-line {{ (!empty($old_date)) ? 'focused' : '' }}">
										                    <input name="dates[]" type="text" class="input-datetime datepicker form-control" value="{{ in_array($subject->id, $subject_ids) ? old('dates.'.$counter) : '' }}" {{ in_array($subject->id, $subject_ids) ? '' : 'disabled' }}>
										                    <label class="form-label">Date - Time</label>
										                </div>
										                @if (in_array($subject->id, $subject_ids))
											                @if ($errors->has('dates.'.$counter))
										                        <label class="error">{{ $errors->first('dates.'.$counter) }}</label>
										                    @endif
									                    @endif
										            </div>
												</td>
											</tr>

											<?php
												if (in_array($subject->id, $subject_ids))
													$counter++;
											?>
	                           			@endforeach
                           			@endif
                           		</tbody>
                           	</table>
                           	<div id="alert" class="alert alert-danger {{ $errors->has('subject_id') ? '' : 'hide' }}">
                           		Select a subject.
		                    </div>
                        </div>
					</div>

					<div class="row clearfix">
                        <div class="col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-4 col-sm-6 col-xs-7">
                            <button type="submit" class="btn btn-primary waves-effect">ADD EXAMINATION</button>
                            <a href="{{ URL::previous() }}" class="btn btn-default waves-effect">CANCEL</a>
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>

<template id="templete-subject">
	<tr>
		<td>
			<input name="subject_id[]" type="checkbox" class="checkbox-subject filled-in chk-col-blue" checked>
			<label class="checkbox-subject-label"></label>
		</td>

		<td>
			<div class="form-group form-float form-group-sm">
                <div class="form-line">
                    <input type="text" name="marks[]" class="input-mark form-control">
                    <label class="form-label">Mark</label>
                </div>
            </div>
		</td>

		<td>
			<div class="form-group form-float form-group-sm">
                <div class="form-line">
                    <input type="text" class="input-datetime datepicker form-control" name="dates[]">
                    <label class="form-label">Date - Time</label>
                </div>
            </div>
		</td>
	</tr>
</template>
@stop

@section('additionalJS')
<script type="text/javascript" src="{{ url('plugins/momentjs/moment.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<script type="text/javascript">
	$(function() {
	    var class_array =<?php echo json_encode($classes); ?>;

	    $('#class_id').change(function() {
	    	var class_index = $("#class_id option:selected").attr('data-class-index');

	    	if (class_index == undefined)
	    		return false;

	    	var subject_array = class_array[class_index].subjects;
	    	$('#content-subjects').html('');
	    	$('#alert').addClass('hide');

	    	if (subject_array.length == 0) {
	    		$('#alert').removeClass('hide');
	    		$('#alert').html('No subject found for this class. <a href="{{ route('show_add_subject') }}" class="alert-link">Click here</a> to add subject.');
	    	} else {
	    		var errors = [];
	    		var length = subject_array.length;

	    		$.each(subject_array, function( index, value ) {
					var html = $('#templete-subject').html();
					var subject = $(html);

					subject.find('.checkbox-subject').attr('id', value.id);
					subject.find('.checkbox-subject').attr('value', value.id);
					subject.find('.checkbox-subject-label').attr('for', value.id);
					subject.find('.checkbox-subject-label').html(value.name);

					$('#content-subjects').append(subject);
				});

				$('.datepicker').bootstrapMaterialDatePicker({
			        format: 'LL - LT',
			        clearButton: true,
			        weekStart: 1,
			        shortTime: true,
			        minDate : new Date(),
			    });

			    $.AdminBSB.input.activate();
	    	}
	    });

	    $('body').on('change', '.checkbox-subject', function() {
	    	if (!$(this).prop('checked')) {
	    		$(this).closest('tr').find('.input-mark').prop('disabled', true);
	    		$(this).closest('tr').find('.input-datetime').prop('disabled', true);
	    	} else {
	    		$(this).closest('tr').find('.input-mark').prop('disabled', false);
	    		$(this).closest('tr').find('.input-datetime').prop('disabled', false);
	    	}
	    });

	    $('.datepicker').bootstrapMaterialDatePicker({
		        format: 'LL LT',
		        clearButton: true,
		        weekStart: 1,
		        shortTime: true,
		    });
	    $.AdminBSB.input.activate();
	});
</script>
@stop