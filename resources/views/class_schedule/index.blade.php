<?php use App\Enumeration\Day; ?>
@extends('layouts.app')

@section('additionalCSS')
<link rel="stylesheet" media="all" type="text/css" href="{{ url('plugins/jquery-ui/jquery-ui.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('plugins/jquery-timepicker/jquery-ui-timepicker-addon.min.css') }}">
<style type="text/css">
	.btn-group{
		margin-bottom: 5px;
	}

	#loading-indicator {
		position: absolute;
		overflow: hidden;
		width: 100%;
		height: 100%;
		background: white;
		z-index: 1000;
	}

	#loading-indicator .preloader {
	    top: calc(50% - 25px);
	    left: calc(50% - 25px);
	}

	.content-schedules {
		width: 85%;
	}
</style>
@stop

@section('content')
@if (sizeof($classes) == 0)
<div class="alert alert-danger">
    <strong>Sorry!</strong> No class found. <a href="{{ route('show_add_class') }}" class="alert-link">Click here</a> to add class. 
</div>
@else
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
		    <div class="body bg-white clearfix">
				<div class="pull-right">
			    	<a href="#" class="btn btn-success btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as PDF">
			            <i class="material-icons">picture_as_pdf</i>
			        </a>

			        <a href="#" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Export table as CSV">
			            <i class="material-icons">insert_drive_file</i>
			        </a>

					<a href="#" class="btn btn-danger btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Print">
			            <i class="material-icons">print</i>
			        </a>
				</div>
		    </div>
		</div>
	</div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
            	<ul class="nav nav-tabs tab-nav-right" role="tablist">
            		@foreach($classes as $class)
                    	<li class="{{ $loop->first ? 'active' : '' }}" role="presentation"><a href="#{{ $class->id }}" data-toggle="tab" aria-expanded="false">{{ $class->name }}</a></li>
                    @endforeach
                </ul>

                <div class="tab-content">
                	@foreach($classes as $class)
	                    <div role="tabpanel" class="tab-pane fade {{ $loop->first ? 'in active' : '' }}" id="{{ $class->id }}">
	                    	@if (sizeof($class->subjects) == 0)
	                    		<div class="alert alert-danger">
	                                No subject found for this class. <a href="{{ route('show_add_subject') }}" class="alert-link">Click here</a> to add subject.
	                            </div>
	                    	@else
								@foreach($class->sections as $section)
					                <h4 class="text-center">
					                    {{ $section->name }}
					                </h4>
						            <table class="table table-bordered" id="{{ $section->id }}">
						            	<tbody>
						            		<tr>
						            			<td>Sunday</td>
						            			<td class="content-schedules">
						            				@foreach($section->schedules as $schedule)
						            					@if ($schedule->day == Day::$SUNDAY)
								            				<div class="btn-group item-schedule">
							                                    <button type="button" class="btn btn-primary waves-effect">
							                                    	<span class="schedule-subject-name">{{ $schedule->subject->name }}</span>
							                                    	<br>
							                                    	<span class="schedule-start-time">{{ $schedule->start }}</span> - <span class="schedule-end-time">{{ $schedule->end }}</span><br>
							                                    	<small><span class="schedule-teacher-name">{{ $schedule->teacher->name }}</span></small>
						                                    	</button>
							                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							                                        <br>
							                                        <span class="caret"></span>
							                                        <span class="sr-only">Toggle Dropdown</span>
							                                        <br>
							                                        <br>
							                                    </button>
							                                    <ul class="dropdown-menu">
							                                        <li><a class=" waves-effect waves-block btn-edit" data-id="{{ $schedule->id }}" data-subject-id="{{ $schedule->subject->id }}" data-teacher-id="{{ $schedule->teacher->id }}"><i class="material-icons">edit</i> Edit</a></li>

					                                                <li><a class=" waves-effect waves-block delete" data-id="{{ $schedule->id }}"><i class="material-icons">delete</i> Delete</a></li>
							                                    </ul>
							                                </div>
						                                @endif
					                                @endforeach
						            			</td>

						            			<td>
						            				<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float btn-add" data-class-index="{{ $loop->parent->index }}" data-day="{{ Day::$SUNDAY }}" data-section-id="{{ $section->id }}" data-class-id="{{ $class->id }}">
					                                    <i class="material-icons">add</i>
					                                </button>
						            			</td>
						            		</tr>

						            		<tr>
						            			<td>Monday</td>
						            			<td class="content-schedules">
			            				   			@foreach($section->schedules as $schedule)
						            					@if ($schedule->day == Day::$MONDAY)
								            				<div class="btn-group item-schedule">
							                                    <button type="button" class="btn btn-primary waves-effect">
							                                    	<span class="schedule-subject-name">{{ $schedule->subject->name }}</span>
							                                    	<br>
							                                    	<span class="schedule-start-time">{{ $schedule->start }}</span> - <span class="schedule-end-time">{{ $schedule->end }}</span><br>
							                                    	<small><span class="schedule-teacher-name">{{ $schedule->teacher->name }}</span></small>
						                                    	</button>
							                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							                                        <br>
							                                        <span class="caret"></span>
							                                        <span class="sr-only">Toggle Dropdown</span>
							                                        <br>
							                                        <br>
							                                    </button>
							                                    <ul class="dropdown-menu">
							                                        <li><a class=" waves-effect waves-block btn-edit" data-id="{{ $schedule->id }}" data-subject-id="{{ $schedule->subject->id }}" data-teacher-id="{{ $schedule->teacher->id }}"><i class="material-icons">edit</i> Edit</a></li>

					                                                <li><a class=" waves-effect waves-block delete" data-id="{{ $schedule->id }}"><i class="material-icons">delete</i> Delete</a></li>
							                                    </ul>
							                                </div>
						                                @endif
					                                @endforeach
						            			</td>

						            			<td>
						            				<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float btn-add" data-class-index="{{ $loop->parent->index }}" data-day="{{ Day::$MONDAY }}" data-section-id="{{ $section->id }}" data-class-id="{{ $class->id }}">
					                                    <i class="material-icons">add</i>
					                                </button>
						            			</td>
						            		</tr>

						            		<tr>
						            			<td>Tuesday</td>
						            			<td class="content-schedules">
				            				   		@foreach($section->schedules as $schedule)
						            					@if ($schedule->day == Day::$TUESDAY)
								            				<div class="btn-group item-schedule">
							                                    <button type="button" class="btn btn-primary waves-effect">
							                                    	<span class="schedule-subject-name">{{ $schedule->subject->name }}</span>
							                                    	<br>
							                                    	<span class="schedule-start-time">{{ $schedule->start }}</span> - <span class="schedule-end-time">{{ $schedule->end }}</span><br>
							                                    	<small><span class="schedule-teacher-name">{{ $schedule->teacher->name }}</span></small>
						                                    	</button>
							                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							                                        <br>
							                                        <span class="caret"></span>
							                                        <span class="sr-only">Toggle Dropdown</span>
							                                        <br>
							                                        <br>
							                                    </button>
							                                    <ul class="dropdown-menu">
							                                        <li><a class=" waves-effect waves-block btn-edit" data-id="{{ $schedule->id }}" data-subject-id="{{ $schedule->subject->id }}" data-teacher-id="{{ $schedule->teacher->id }}"><i class="material-icons">edit</i> Edit</a></li>

					                                                <li><a class=" waves-effect waves-block delete" data-id="{{ $schedule->id }}"><i class="material-icons">delete</i> Delete</a></li>
							                                    </ul>
							                                </div>
						                                @endif
					                                @endforeach
						            			</td>

						            			<td>
						            				<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float btn-add" data-class-index="{{ $loop->parent->index }}" data-day="{{ Day::$TUESDAY }}" data-section-id="{{ $section->id }}" data-class-id="{{ $class->id }}">
					                                    <i class="material-icons">add</i>
					                                </button>
						            			</td>
						            		</tr>

						            		<tr>
						            			<td>Wednesday</td>
						            			<td class="content-schedules">
				            				   		@foreach($section->schedules as $schedule)
						            					@if ($schedule->day == Day::$WEDNESDAY)
								            				<div class="btn-group item-schedule">
							                                    <button type="button" class="btn btn-primary waves-effect">
							                                    	<span class="schedule-subject-name">{{ $schedule->subject->name }}</span>
							                                    	<br>
							                                    	<span class="schedule-start-time">{{ $schedule->start }}</span> - <span class="schedule-end-time">{{ $schedule->end }}</span><br>
							                                    	<small><span class="schedule-teacher-name">{{ $schedule->teacher->name }}</span></small>
						                                    	</button>
							                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							                                        <br>
							                                        <span class="caret"></span>
							                                        <span class="sr-only">Toggle Dropdown</span>
							                                        <br>
							                                        <br>
							                                    </button>
							                                    <ul class="dropdown-menu">
							                                        <li><a class=" waves-effect waves-block btn-edit" data-id="{{ $schedule->id }}" data-subject-id="{{ $schedule->subject->id }}" data-teacher-id="{{ $schedule->teacher->id }}"><i class="material-icons">edit</i> Edit</a></li>

					                                                <li><a class=" waves-effect waves-block delete" data-id="{{ $schedule->id }}"><i class="material-icons">delete</i> Delete</a></li>
							                                    </ul>
							                                </div>
						                                @endif
					                                @endforeach
						            			</td>

						            			<td>
						            				<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float btn-add" data-class-index="{{ $loop->parent->index }}" data-day="{{ Day::$WEDNESDAY }}" data-section-id="{{ $section->id }}" data-class-id="{{ $class->id }}">
					                                    <i class="material-icons">add</i>
					                                </button>
						            			</td>
						            		</tr>

						            		<tr>
						            			<td>Thursday</td>
						            			<td class="content-schedules">
					            					@foreach($section->schedules as $schedule)
						            					@if ($schedule->day == Day::$THURSDAY)
								            				<div class="btn-group item-schedule">
							                                    <button type="button" class="btn btn-primary waves-effect">
							                                    	<span class="schedule-subject-name">{{ $schedule->subject->name }}</span>
							                                    	<br>
							                                    	<span class="schedule-start-time">{{ $schedule->start }}</span> - <span class="schedule-end-time">{{ $schedule->end }}</span><br>
							                                    	<small><span class="schedule-teacher-name">{{ $schedule->teacher->name }}</span></small>
						                                    	</button>
							                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							                                        <br>
							                                        <span class="caret"></span>
							                                        <span class="sr-only">Toggle Dropdown</span>
							                                        <br>
							                                        <br>
							                                    </button>
							                                    <ul class="dropdown-menu">
							                                        <li><a class=" waves-effect waves-block btn-edit" data-id="{{ $schedule->id }}" data-subject-id="{{ $schedule->subject->id }}" data-teacher-id="{{ $schedule->teacher->id }}"><i class="material-icons">edit</i> Edit</a></li>

					                                                <li><a class=" waves-effect waves-block delete" data-id="{{ $schedule->id }}"><i class="material-icons">delete</i> Delete</a></li>
							                                    </ul>
							                                </div>
						                                @endif
					                                @endforeach   
						            			</td>

						            			<td>
						            				<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float btn-add" data-class-index="{{ $loop->parent->index }}" data-day="{{ Day::$THURSDAY }}" data-section-id="{{ $section->id }}" data-class-id="{{ $class->id }}">
					                                    <i class="material-icons">add</i>
					                                </button>
						            			</td>
						            		</tr>

						            		<tr>
						            			<td>Friday</td>
						            			<td class="content-schedules">
				            				   		@foreach($section->schedules as $schedule)
						            					@if ($schedule->day == Day::$FRIDAY)
								            				<div class="btn-group item-schedule">
							                                    <button type="button" class="btn btn-primary waves-effect">
							                                    	<span class="schedule-subject-name">{{ $schedule->subject->name }}</span>
							                                    	<br>
							                                    	<span class="schedule-start-time">{{ $schedule->start }}</span> - <span class="schedule-end-time">{{ $schedule->end }}</span><br>
							                                    	<small><span class="schedule-teacher-name">{{ $schedule->teacher->name }}</span></small>
						                                    	</button>
							                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							                                        <br>
							                                        <span class="caret"></span>
							                                        <span class="sr-only">Toggle Dropdown</span>
							                                        <br>
							                                        <br>
							                                    </button>
							                                    <ul class="dropdown-menu">
							                                        <li><a class=" waves-effect waves-block btn-edit" data-id="{{ $schedule->id }}" data-subject-id="{{ $schedule->subject->id }}" data-teacher-id="{{ $schedule->teacher->id }}"><i class="material-icons">edit</i> Edit</a></li>

					                                                <li><a class=" waves-effect waves-block delete" data-id="{{ $schedule->id }}"><i class="material-icons">delete</i> Delete</a></li>
							                                    </ul>
							                                </div>
						                                @endif
					                                @endforeach
						            			</td>

						            			<td>
						            				<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float btn-add" data-class-index="{{ $loop->parent->index }}" data-day="{{ Day::$FRIDAY }}" data-section-id="{{ $section->id }}" data-class-id="{{ $class->id }}">
					                                    <i class="material-icons">add</i>
					                                </button>
						            			</td>
						            		</tr>

						            		<tr>
						            			<td>Saturday</td>
						            			<td class="content-schedules">
				            				   		@foreach($section->schedules as $schedule)
						            					@if ($schedule->day == Day::$SATURDAY)
								            				<div class="btn-group item-schedule">
							                                    <button type="button" class="btn btn-primary waves-effect">
							                                    	<span class="schedule-subject-name">{{ $schedule->subject->name }}</span>
							                                    	<br>
							                                    	<span class="schedule-start-time">{{ $schedule->start }}</span> - <span class="schedule-end-time">{{ $schedule->end }}</span><br>
							                                    	<small><span class="schedule-teacher-name">{{ $schedule->teacher->name }}</span></small>
						                                    	</button>
							                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							                                        <br>
							                                        <span class="caret"></span>
							                                        <span class="sr-only">Toggle Dropdown</span>
							                                        <br>
							                                        <br>
							                                    </button>
							                                    <ul class="dropdown-menu">
							                                        <li><a class=" waves-effect waves-block btn-edit" data-id="{{ $schedule->id }}" data-subject-id="{{ $schedule->subject->id }}" data-teacher-id="{{ $schedule->teacher->id }}"><i class="material-icons">edit</i> Edit</a></li>

					                                                <li><a class=" waves-effect waves-block delete" data-id="{{ $schedule->id }}"><i class="material-icons">delete</i> Delete</a></li>
							                                    </ul>
							                                </div>
						                                @endif
					                                @endforeach
						            			</td>

						            			<td>
						            				<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float btn-add" data-class-index="{{ $loop->parent->index }}" data-day="{{ Day::$SATURDAY }}" data-section-id="{{ $section->id }}" data-class-id="{{ $class->id }}">
					                                    <i class="material-icons">add</i>
					                                </button>
						            			</td>
						            		</tr>
						            	</tbody>
						            </table>
					            @endforeach
				            @endif
	                    </div>
                    @endforeach
           		</div>
       		</div>
        </div>
    </div>
</div>

<template id="templete-schedule">
	<div class="btn-group item-schedule" style="margin-right: 4px">
        <button type="button" class="btn btn-primary waves-effect">
        	<span class="schedule-subject-name"></span><br>
        	<span class="schedule-start-time"></span> - 
        	<span class="schedule-end-time"></span><br>
        	<small><span class="schedule-teacher-name"></span></small>
    	</button>
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <br>
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
            <br>
            <br>
        </button>
        <ul class="dropdown-menu">
            <li><a class="waves-effect waves-block btn-edit"><i class="material-icons">edit</i> Edit</a></li>

            <li><a class=" waves-effect waves-block delete" data-id="#"><i class="material-icons">delete</i> Delete</a></li>
        </ul>
    </div>
</template>

<div class="modal fade"  role="dialog" id="schedule-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div id="loading-indicator">
				<div class="preloader">
	                <div class="spinner-layer pl-light-blue">
	                    <div class="circle-clipper left">
	                        <div class="circle"></div>
	                    </div>
	                    <div class="circle-clipper right">
	                        <div class="circle"></div>
	                    </div>
	                </div>
	            </div>
			</div>
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Add Schedule</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="col-md-6">
						<div class="form-group">
							<label for="section-subject" class="control-label">Subject:</label>
	                        <select class="form-control show-tick" id="select-subject" title="Choose subject">
							</select>
							<label class="error" id="subject-error"></label>
	                    </div>

						<div class="form-group clockpicker">
	                    	<label for="start_time" class="control-label">Start Time:</label>
	                    	<div class="form-line" id="form-line-start-time">
						    	<input id="input-start-time" name="start_time" type="text" class="form-control" value="09:00 AM">
					    	</div>
					    	<label class="error" id="start-time-error"></label>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="select-teacher" class="control-label">Teacher:</label>
							<select class="form-control show-tick" id="select-teacher" title="Choose teacher">
							</select>
							<label class="error" id="teacher-error"></label>
						</div>

						<div class="form-group clockpicker">
	                    	<label for="end_time" class="control-label">End Time:</label>
	                    	<div class="form-line" id="form-line-end-time">
						    	<input id="input-end-time" name="end_time" type="text" class="form-control" value="09:30 AM">
					    	</div>
					    	<label class="error" id="end-time-error"></label>
						</div>
					</div>
				</form>
				
				<div class="col-md-12">
					<div id="modal-error" class="alert alert-danger hide"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="btn-add-modal" class="btn btn-link waves-effect">ADD</button>
				<button type="button" id="btn-save-modal" class="btn btn-link waves-effect hide">SAVE</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-red">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Delete Schedule</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <button id="btn-delete" type="button" class="btn btn-link waves-effect">DELETE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
@endif
@stop

@section('additionalJS')
<script type="text/javascript" src="{{ url('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/jquery-timepicker/jquery-ui-timepicker-addon.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/jquery-timepicker/jquery-ui-sliderAccess.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/momentjs/moment.js') }}"></script>
<script>
	$(function() {
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		var class_array =<?php echo json_encode($classes ); ?>;
		var subject_array;

		var input_start_time = $('#input-start-time');
		var input_end_time = $('#input-end-time');
		
		$.timepicker.timeRange(
			input_start_time,
			input_end_time,
			{
				minInterval: (1000*60*10), // 1hr
				timeFormat: 'hh:mm TT',
			}
		);

		$('.btn-add').click(function() {
			var class_index = $(this).data('class-index');
			subject_array = class_array[class_index].subjects;
			
			$('#select-subject').html('');
			$.each(subject_array, function( index, value ) {
				$('#select-subject').append('<option value="'+value.id+'" data-suject-index="'+index+'">'+value.name+'</option>');
			});
			$('#select-subject').selectpicker('refresh');

			var day = $(this).data('day');
			var section_id = $(this).data('section-id');
			var class_id = $(this).data('class-id');
			var tr_index = $(this).closest('tr').index();
			$('#btn-add-modal').attr('data-day', day);
			$('#btn-add-modal').attr('data-section-id', section_id);
			$('#btn-add-modal').attr('data-class-id', class_id);
			$('#btn-add-modal').attr('data-tr-index', tr_index);
			
			$('#loading-indicator').hide();
			$('#schedule-modal').modal('show');
		});

		$('#select-subject').change(function() {
			var subject_index = $(this).find('option:selected').data('suject-index');
			var teacher_array = subject_array[subject_index].teachers;

			$('#select-teacher').html('');
			$.each(teacher_array, function( index, value ) {
				$('#select-teacher').append('<option value="'+value.id+'">'+value.name+'</option>');
			});
			$('#select-teacher').selectpicker('refresh');
		});

		$('#schedule-modal').on('hide.bs.modal', function (e) {
			$('#subject-error').html('');
			$('#teacher-error').html('');
			$('#start-time-error').html('');
			$('#end-time-error').html('');
			$('#input-start-time').val('09:00 AM');
			$('#input-end-time').val('09:30 AM');
			$('#select-teacher').html('');
			$('#select-teacher').selectpicker('render');
			$('#modal-error').addClass('hide');
			$('#btn-save-modal').addClass('hide');
			$('#btn-add-modal').removeClass('hide');
			$('#schedule-modal').find('.modal-title').html('Add Schedule');
		});

		$('#btn-add-modal').click(function() {
			var error = false;
			$('#subject-error').html('');
			$('#teacher-error').html('');
			$('#start-time-error').html('');
			$('#end-time-error').html('');
			$('#modal-error').addClass('hide');

			if ($('#select-subject').val() == ''){
				error = true;
				$('#subject-error').html('Select a subject.');
			}

			if (!$('#select-teacher').val() || $('#select-teacher').val() == ''){
				error = true;
				$('#teacher-error').html('Select a teacher.');
			}

			if ($('#input-start-time').val() == ''){
				error = true;
				$('#start-time-error').html('Select start time.');
			}

			if ($('#input-end-time').val() == ''){
				error = true;
				$('#end-time-error').html('Select end time.');
			}

			if (!error) {
				var moment_start_time = moment($('#input-start-time').val(), "hh:mm A");
				var moment_end_time = moment($('#input-end-time').val(), "hh:mm A");
				var tr_index = $(this).attr('data-tr-index');

				var request_data = {
						teacher_id : $('#select-teacher').val(),
						subject_id : $('#select-subject').val(),
						start_time : moment_start_time.format("HH:mm:ss"),
						end_time : moment_end_time.format("HH:mm:ss"),
						day : $(this).attr('data-day'),
						class_id : $(this).data('class-id'),
						section_id : $(this).data('section-id'),
					};

				$('#loading-indicator').show();

				$.ajax({
					method: "POST",
					url: "{{ route('add_class_schedule') }}",
					data: request_data,
				}).done(function( data ) {
					if (!data.success) {
						$('#modal-error').html(data.message);
						$('#modal-error').removeClass('hide');
					} else {
						var html = $('#templete-schedule').html();
						var templete_schedule = $(html);
						var subject_name = $("#select-subject option:selected").text();
						var teacher_name = $("#select-teacher option:selected").text();
						var start_time = $('#input-start-time').val();
						var end_time = $('#input-end-time').val();

						templete_schedule.find('.schedule-subject-name').html(subject_name);
						templete_schedule.find('.schedule-teacher-name').html(teacher_name);
						templete_schedule.find('.schedule-start-time').html(start_time);
						templete_schedule.find('.schedule-end-time').html(end_time);
						templete_schedule.find('.btn-edit').attr('data-id', data.message);
						templete_schedule.find('.btn-edit').attr('data-subject-id', request_data['subject_id']);
						templete_schedule.find('.btn-edit').attr('data-teacher-id', request_data['teacher_id']);
						templete_schedule.find('.delete').attr('data-id', data.message);

						add_schedule_item(templete_schedule, request_data['section_id'], tr_index, moment_start_time);
						$('#schedule-modal').modal('hide');
					}
					$('#loading-indicator').hide();
				});
			}
		});

		function add_schedule_item(templete_schedule, section_id, tr_index, moment_start_time) {
			var found = false;

			$('#'+section_id)
				.find('tr:eq('+tr_index+')')
				.find('.item-schedule').each(function(index) {
					moment_obj = moment($(this).find('.schedule-start-time').html(), "hh:mm A");
			    	if (moment_start_time.isBefore(moment_obj)) {
			    		found = true;
			    		templete_schedule.insertBefore(this);
			    		return false;
			    	}
				});

			if (!found) {
				$('#'+section_id)
					.find('tr:eq('+tr_index+')')
					.find('.content-schedules')
					.append(templete_schedule);
			}
		}


		// Edit Schedule
		$('body').on('click','.btn-edit',function(){
		    var schedule_id = $(this).data('id');
		    var subject_id = $(this).data('subject-id');
		    var teacher_id = $(this).data('teacher-id');
		    var start_time = $(this).closest('.item-schedule').find('.schedule-start-time').html();
		    var end_time = $(this).closest('.item-schedule').find('.schedule-end-time').html();
 
		    var class_index = $(this).closest('tr').find('.btn-add').data('class-index');
			subject_array = class_array[class_index].subjects;
			
			$('#select-subject').html('');
			$.each(subject_array, function( index, value ) {
				if (subject_id == value.id) {
					$('#select-subject').append('<option value="'+value.id+'" data-suject-index="'+index+'" selected>'+value.name+'</option>');
				} else {
					$('#select-subject').append('<option value="'+value.id+'" data-suject-index="'+index+'">'+value.name+'</option>');
				}
			});
			$('#select-subject').selectpicker('refresh');
			$('#select-subject').trigger('change');
			$('#select-teacher').val(teacher_id);
			$('#select-teacher').selectpicker('refresh');

			$('#input-start-time').val(start_time);
			$('#input-end-time').val(end_time);
			$('#btn-save-modal').removeClass('hide');
			$('#btn-add-modal').addClass('hide');
			$('#schedule-modal').find('.modal-title').html('Edit Schedule');
			
			var day = $(this).closest('tr').find('.btn-add').data('day');
			var section_id = $(this).closest('tr').find('.btn-add').data('section-id');
			var class_id = $(this).closest('tr').find('.btn-add').data('class-id');
			var item_schedule_index = $(".btn-edit").index(this);
			$('#btn-save-modal').attr('data-id', schedule_id);
			$('#btn-save-modal').attr('data-day', day);
			$('#btn-save-modal').attr('data-section-id', section_id);
			$('#btn-save-modal').attr('data-class-id', class_id);
			$('#btn-save-modal').attr('data-item-schedule-index', item_schedule_index);

			$('#loading-indicator').hide();
			$('#schedule-modal').modal('show');
		});

		$('#btn-save-modal').click(function() {
			var error = false;
			$('#subject-error').html('');
			$('#teacher-error').html('');
			$('#start-time-error').html('');
			$('#end-time-error').html('');
			$('#modal-error').addClass('hide');

			if ($('#select-subject').val() == ''){
				error = true;
				$('#subject-error').html('Select a subject.');
			}

			if (!$('#select-teacher').val() || $('#select-teacher').val() == ''){
				error = true;
				$('#teacher-error').html('Select a teacher.');
			}

			if ($('#input-start-time').val() == ''){
				error = true;
				$('#start-time-error').html('Select start time.');
			}

			if ($('#input-end-time').val() == ''){
				error = true;
				$('#end-time-error').html('Select end time.');
			}

			if (!error) {
				var moment_start_time = moment($('#input-start-time').val(), "hh:mm A");
				var moment_end_time = moment($('#input-end-time').val(), "hh:mm A");
				var item_schedule_index = $(this).attr('data-item-schedule-index');

				var request_data = {
						id: $(this).attr('data-id'),
						teacher_id : $('#select-teacher').val(),
						subject_id : $('#select-subject').val(),
						start_time : moment_start_time.format("HH:mm:ss"),
						end_time : moment_end_time.format("HH:mm:ss"),
						day : $(this).attr('data-day'),
						class_id : $(this).data('class-id'),
						section_id : $(this).data('section-id'),
					};

				$('#loading-indicator').show();

				$.ajax({
					method: "POST",
					url: "{{ route('edit_class_schedule') }}",
					data: request_data,
				}).done(function( data ) {
					if (!data.success) {
						$('#modal-error').html(data.message);
						$('#modal-error').removeClass('hide');
					} else {
						var subject_name = $("#select-subject option:selected").text();
						var teacher_name = $("#select-teacher option:selected").text();
						var start_time = $('#input-start-time').val();
						var end_time = $('#input-end-time').val();

						$('.item-schedule:eq('+item_schedule_index+')').find('.schedule-subject-name').html(subject_name);
						$('.item-schedule:eq('+item_schedule_index+')').find('.schedule-teacher-name').html(teacher_name);
						$('.item-schedule:eq('+item_schedule_index+')').find('.schedule-start-time').html(start_time);
						$('.item-schedule:eq('+item_schedule_index+')').find('.schedule-end-time').html(end_time);
						$('#schedule-modal').modal('hide');
					}
					$('#loading-indicator').hide();
				});
			}
		});

		// Delete
		$('body').on('click','.delete',function(){
            var id = $(this).data('id');
            var item_schedule_index = $(".delete").index(this);
            $('#btn-delete').attr("data-id", id);
            $('#btn-delete').attr("data-item-schedule-index", item_schedule_index);
            $('#delete-modal').modal('show');
        });

        $('#btn-delete').click(function() {
            var id = $(this).data('id');
            var item_schedule_index = $(this).data('item-schedule-index');

            $.ajax({
                method: "POST",
                url: "{{ route('delete_class_schedule') }}",
                data: { id: id}
            }).done(function( msg ) {
                $('.item-schedule:eq('+item_schedule_index+')').remove();
                $('#delete-modal').modal('hide');
            });
        });
	});
</script>
@stop