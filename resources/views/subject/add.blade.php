@extends('layouts.app')

@section('additionalCSS')
<style type="text/css">
	.borderless td, .borderless tr {
	    border: none !important;
	    padding: 0px !important;
	}
</style>
@stop

@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
	            <h2>
	                Subject Information
	            </h2>
	        </div>

	        <div class="body">
				<form class="form-horizontal" action="{{ route('post_add_subject') }}" method="POST" id="class-form">
					{{ csrf_field() }}
					<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="name">Name</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line {{ $errors->has('name') ? ' focused error' : '' }}" id="form-line-name">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter subject name" value="{{ old('name') }}">
                                </div>
		                        @if ($errors->has('name'))
			                        <label class="error">{{ $errors->first('name') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="name">Class</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                        	<div class="form-group">
                        		<select name="class_id" class="form-control show-tick" title="Choose Class">
	                            	@foreach($classes as $class)
	                            		<option value="{{ $class->id }}" {{ $class->id == old('class_id') ? 'selected' : '' }}>{{ $class->name }}</option>
	                            	@endforeach
	                            </select>
	                            @if ($errors->has('class_id'))
			                        <label class="error">{{ $errors->first('class_id') }}</label>
			                    @endif
                        	</div>
                        </div>
                    </div>

					<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="sections">Teacher(s)</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                			<table class="table borderless" id="teacher-group">
                				@if (old('teacher_id'))
                					@foreach(old('teacher_id') as $item)
                						<tr>
		                					<td>
		                						<div class="form-group">
		                    						<select name="teacher_id[]" class="form-control show-tick " title="Choose Teacher" data-live-search="true">
						                            	@foreach($teachers as $teacher)
						                            		<option value="{{ $teacher->id }}" {{ $teacher->id == $item ? 'selected' : '' }}>{{ $teacher->name }}</option>
						                            	@endforeach
						                            </select>

						                            @if ($errors->has('teacher_id.'.$loop->index))
								                        <label class="error">{{ $errors->first('teacher_id.'.$loop->index) }}</label>
								                    @endif
					                            </div>
		                					</td>

		                					<td class="td-remove-teacher"></td>
		                				</tr>
                					@endforeach
                				@else
	                				<tr>
	                					<td>
	                						<div class="form-group">
	                    						<select name="teacher_id[]" class="form-control show-tick " title="Choose Teacher" data-live-search="true">
					                            	@foreach($teachers as $teacher)
					                            		<option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
					                            	@endforeach
					                            </select>
				                            </div>
	                					</td>

	                					<td class="td-remove-teacher">
	                						
	                					</td>
	                				</tr>
                				@endif
                			</table>
							<a id="btn-add-teacher" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Add Teacher"><i class="material-icons">add</i></a>
						</div>
					</div>

					<div class="row clearfix">
                        <div class="col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-4 col-sm-6 col-xs-7">
                            <button type="submit" class="btn btn-primary waves-effect">ADD CLASS</button>
                            <a href="{{ URL::previous() }}" class="btn btn-default waves-effect">CANCEL</a>
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>

<template id="teacher-item">
	<tr>
		<td>
			<div class="form-group">
				<select name="teacher_id[]" class="form-control show-tick teacher_id" title="Choose Teacher" data-live-search="true">
		        	@foreach($teachers as $teacher)
		        		<option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
		        	@endforeach
		        </select>
			</div>
		</td>

		<td class="td-remove-teacher">
			
		</td>
	</tr>
</template>

<template id="teacher-delete-item">
	<a role="button" class="btn-remove-teacher">
		<i class="material-icons">delete</i>
	</a>
</template>

<div class="modal fade" id="alert-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-red">
            <div class="modal-header">
                <h4 class="modal-title">{{ config('app.name', 'Laravel') }}</h4>
            </div>
            <div class="modal-body">
                <p id="alert-message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script>
	$(function() {
		var teachers = <?php echo json_encode($teachers); ?>;
		check_delete();

		$('#btn-add-teacher').click(function() {
			var html = $('#teacher-item').html();
			var row = $(html);
			$('#teacher-group').append(row);
			$('.teacher_id:last').selectpicker('render');

			check_delete();
		});

		$(document).on('click', '.btn-remove-teacher', function() {
			$(this).closest('tr').remove();

			check_delete();
		});

		function check_delete() {
			if ($('.td-remove-teacher').length > 1) {
				var html = $('#teacher-delete-item').html();
				var row = $(html);
				$('.td-remove-teacher').html(row);
			} else if ($('.td-remove-teacher').length < 2) {
				$('.td-remove-teacher').html('');
			}
		}
	});
</script>
@stop