@extends('layouts.app')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2>
                    LIST OF SUBJECTS 
                    <span class="pull-right">
                    	<a href="{{ route('show_add_subject') }}" class="btn btn-primary btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Add Subject">
				            <i class="material-icons">add</i>
				    	</a>
                    </span>
                </h2>
            </div>

            <div class="body">
                @if (sizeof($classes) > 0)
                	<ul class="nav nav-tabs tab-nav-right" role="tablist">
                        @foreach($classes as $class)
                            <li class="{{ $loop->first ? 'active' : '' }}" role="presentation"><a href="#{{ $class->id }}" data-toggle="tab" aria-expanded="false">{{ $class->name }}</a></li>
                        @endforeach
                    </ul>

                	<div class="tab-content">
                        @foreach($classes as $class)
                            <div role="tabpanel" class="tab-pane fade {{ $loop->first ? 'in active' : '' }}" id="{{ $class->id }}">
                                @if(sizeof($class->subjects) > 0)
                                <table class="table table-bordered" id="table-subjects">
                                	<thead>
                                		<tr>
                                			<th>Name</th>
                                			<th>Teacher(s)</th>
                                			<th></th>
                                		</tr>
                                	</thead>

                                    <tbody>
                                        @foreach($class->subjects as $subject)
                                            @foreach($subject->teachers as $teacher)
                                                @if ($loop->first)
                                                    <tr>
                                                        <td rowspan="{{ sizeof($subject->teachers) }}">{{ $subject->name }}</td>
                                                        <td>{{ $teacher->name }}</td>
                                                        <td class="text-right" rowspan="{{ sizeof($subject->teachers) }}">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn bg-cyan dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                     <i class="material-icons">more_vert</i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-right">
                                                                    <li><a href="{{ route('show_edit_subject', ['id' => $subject->id]) }}" class=" waves-effect waves-block"><i class="material-icons">edit</i> Edit</a></li>

                                                                    <li><a class=" waves-effect waves-block delete" data-id="{{ $subject->id }}" data-row-count="{{ sizeof($subject->teachers) }}"><i class="material-icons">delete</i> Delete</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{ $teacher->name }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                    <div class="alert alert-danger">
                                        No subject added. <a href="{{ route('show_add_subject') }}" class="alert-link">Click here </a>to add subject.
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="alert alert-danger">
                        No class added. <a href="{{ route('show_add_class') }}" class="alert-link">Click here </a>to add class.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-red">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Delete Subject</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <button id="btn-delete" type="button" class="btn btn-link waves-effect">DELETE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete').click(function() {
            var id = $(this).data('id');
            var row_count = $(this).data('row-count');
            var index = $(this).closest('tr').index('tr');
            $('#btn-delete').attr("data-id", id);
            $('#btn-delete').attr("data-index", index);
            $('#btn-delete').attr("data-row-count", row_count);
            $('#delete-modal').modal('show');
        });

        $('#btn-delete').click(function() {
            var id = $(this).data('id');
            var index = $(this).data('index');
            var row_count = $(this).data('row-count');

            $.ajax({
                method: "POST",
                url: "{{ route('delete_subject') }}",
                data: { id: id}
            }).done(function( msg ) {
                for(i=1; i<row_count; i++)
                    $('#table-subjects tr:eq('+index+')').next().remove();

                $('#table-subjects tr:eq('+index+')').remove();
                $('#delete-modal').modal('hide');
            });
        });
    });
</script>
@stop