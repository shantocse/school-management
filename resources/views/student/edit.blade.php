@extends('layouts.app')

@section('additionalCSS')
<link rel="stylesheet" type="text/css" href="{{ url('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('plugins/intl-tel-input/css/intlTelInput.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('plugins/cropperjs/cropper.min.css') }}">
@stop

@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
	            <h2>
	                Student Information
	            </h2>
	        </div>

			<form id="form-student" class="form-horizontal" action="{{ route('post_edit_student',['id' => $student->id]) }}" method="POST">
	        	<div class="body">
	        	
	        		{{ csrf_field() }}
	        		<input type="hidden" name="redirect_url" value="{{ URL::previous() }}">
	        		<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="name">Name</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('name') ? ' focused error' : '' }}">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ old('name') ? old('name') : $student->meta->name }}">
                                </div>
                                @if ($errors->has('name'))
			                        <label class="error">{{ $errors->first('name') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="username">Username</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('username') ? ' focused error' : '' }}">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Enter username" value="{{ old('username') ? old('username') : $student->meta->username }}">
                                </div>
                                @if ($errors->has('username'))
			                        <label class="error">{{ $errors->first('username') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="roll">Roll</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('roll') ? ' focused error' : '' }}">
                                    <input type="text" class="form-control" name="roll" id="roll" placeholder="Enter roll" value="{{ old('roll') ? old('roll') : $student->roll }}">
                                </div>
                                @if ($errors->has('roll'))
			                        <label class="error">{{ $errors->first('roll') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('email') ? ' focused error' : '' }}">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ old('email') ? old('email') : $student->meta->email }}">
                                </div>
                                @if ($errors->has('email'))
			                        <label class="error">{{ $errors->first('email') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="gender">Gender</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <input type="radio" class="with-gap radio-col-blue" name="gender" id="gender_male" value="male" {{ $student->meta->gender == 'male' ? 'checked' : '' }}>
                        		<label for="gender_male">Male</label>

                        		<input type="radio" class="with-gap radio-col-blue" name="gender" id="gender_female" value="female" {{ $student->meta->gender == 'female' ? 'checked' : '' }}>
                        		<label for="gender_female">Female</label>
								
								@if ($errors->has('gender'))
			                        <label class="error">{{ $errors->first('gender') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="dob">Birthday</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('dob') ? ' focused error' : '' }}">
                                	<input type="text" name="dob" data-provide="datepicker" data-date-format="dd-mm-yyyy" class="datepicker form-control" id="dob" placeholder="Please choose a date" value="{{ old('dob') ? old('dob') : $student->meta->dob }}">
                                </div>
                                @if ($errors->has('dob'))
			                        <label class="error">{{ $errors->first('dob') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="phone">Phone</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div id="form-line-phone" class="form-line{{ $errors->has('phone') ? ' focused error' : '' }}">
                                	<input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone-full') ? old('phone-full') : $student->meta->phone }}">
									<input id="phone-full" type="hidden" name="phone-full">
                                </div>
                                <label id="phone-error" class="error">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="class_id">Class</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <select class="form-control show-tick" id="class_id" name="class_id" title="Choose class">
									@foreach($classes as $class)
										<option value="{{ $class['id'] }}" {{ $student->class_id == $class['id'] ? 'selected' : '' }}>{{ $class['name'] }}</option>
									@endforeach
								</select>
								@if ($errors->has('class_id'))
			                        <label class="error">{{ $errors->first('class_id') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="section_id">Section</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <select class="form-control show-tick" id="section_id" name="section_id" title="Choose section">

								</select>
								@if ($errors->has('section_id'))
			                        <label class="error">{{ $errors->first('section_id') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="transportation_id">Transportation</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <select class="form-control" id="transportation_id" name="transportation_id">
									<option value="0">No Transportation</option>
								</select>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="dob">Photo</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <img id="photo_preview" class="img-responsive img-circle" src="{{ old('photo_data') ? old('photo_data') : route('profile_pic', ['id' => $student->meta->id]) }}" width="50px">
							<input class="hide" type="file" accept="image/*" name="photo" id="photo">
							<input type="hidden" name="photo_data" id="photo_data" value="{{ old('photo_data') }}">
							<br>

							<a id="change_photo" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Select photo">
                                <i class="material-icons">image</i>
                            </a>

                            <a id="reset_photo" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Reset">
                                <i class="material-icons">refresh</i>
                            </a>
                        </div>
                    </div>
	        	</div> <!-- body -->

	        	<div class="header">
		            <h2>
		                Guardian Information
		            </h2>
		        </div>

		        <div class="body">
		        	<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="guardian_type">Type</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <input type="radio" class="with-gap radio-col-blue guardian_type" name="guardian_type" id="guardian_type_existing" value="existing" {{ old('guardian_type') == 'existing' ? 'checked' : '' }} {{ old('guardian_type') ? '' : 'checked' }}>
                        		<label for="guardian_type_existing">Existing</label>

                        		<input type="radio" class="with-gap radio-col-blue guardian_type" name="guardian_type" id="guardian_type_new" value="new" {{ old('guardian_type') == 'new' ? 'checked' : '' }}>
                        		<label for="guardian_type_new">New</label>
                            </div>
                        </div>
                    </div>

		        	<div class="row clearfix">
                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="relation">Relation</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line{{ $errors->has('relation') ? ' focused error' : '' }}">
                                    <input type="text" name="relation" class="form-control" id="relation" placeholder="Enter relation" value="{{ old('relation') ? old('relation') : $student->guardian_relation }}">
                                </div>
                                @if ($errors->has('relation'))
			                        <label class="error">{{ $errors->first('relation') }}</label>
			                    @endif
                            </div>
                        </div>
                    </div>

                    <div id="existing_guardian">
                    	<div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_id">Guardian</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <select class="form-control show-tick" name="guardian_id" id="guardian_id" data-live-search="true" title="Choose guardian">
										@foreach($guardians as $guardian)
											<option value="{{ $guardian->id }}" {{ $student->guardian_id == $guardian->id ? 'selected' : '' }}>{{ $guardian->name }}</option>
										@endforeach
									</select>
									@if ($errors->has('guardian_id'))
				                        <label class="error">{{ $errors->first('guardian_id') }}</label>
				                    @endif
	                            </div>
	                        </div>
	                    </div>
                    </div><!-- Existing -->

                    <div class="hide" id="new_guardian">
                    	<div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_name">Name</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line{{ $errors->has('guardian_name') ? ' focused error' : '' }}">
	                                    <input type="text" name="guardian_name" class="form-control" id="guardian_name" placeholder="Enter guardian name" value="{{ old('guardian_name') }}">
	                                </div>
	                                @if ($errors->has('guardian_name'))
				                        <label class="error">{{ $errors->first('guardian_name') }}</label>
				                    @endif
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_username">Username</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line{{ $errors->has('guardian_username') ? ' focused error' : '' }}">
	                                    <input type="text" class="form-control" name="guardian_username" id="guardian_username" placeholder="Enter guardian username" value="{{ old('guardian_username') }}">
	                                </div>
	                                @if ($errors->has('guardian_username'))
				                        <label class="error">{{ $errors->first('guardian_username') }}</label>
				                    @endif
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_email">Email</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line{{ $errors->has('guardian_email') ? ' focused error' : '' }}">
	                                    <input type="email" name="guardian_email" class="form-control" id="guardian_email" placeholder="Enter guardian email" value="{{ old('guardian_email') }}">
	                                </div>
	                                @if ($errors->has('guardian_email'))
				                        <label class="error">{{ $errors->first('guardian_email') }}</label>
				                    @endif
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_password">Password</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line{{ $errors->has('guardian_password') ? ' focused error' : '' }}">
	                                    <input type="password" name="guardian_password" class="form-control" id="guardian_password" placeholder="Enter guardian password">
	                                </div>
	                                @if ($errors->has('guardian_password'))
				                        <label class="error">{{ $errors->first('guardian_password') }}</label>
				                    @endif
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_password-confirm">Confirm Password</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <div class="form-line">
	                                    <input type="password" name="guardian_password_confirmation" class="form-control" id="guardian_password-confirm" placeholder="Confirm guardian password">
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_gender">Gender</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <input type="radio" class="with-gap radio-col-blue" name="guardian_gender" id="guardian_gender_male" value="male" {{ old('guardian_gender') == 'male' ? 'checked' : '' }}>
	                        		<label for="guardian_gender_male">Male</label>

	                        		<input type="radio" class="with-gap radio-col-blue" name="guardian_gender" id="guardian_gender_female" value="female" {{ old('guardian_gender') == 'female' ? 'checked' : '' }}>
	                        		<label for="guardian_gender_female">Female</label>
									
									@if ($errors->has('guardian_gender'))
				                        <label class="error">{{ $errors->first('guardian_gender') }}</label>
				                    @endif
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_phone">Phone</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <div class="form-group">
	                                <div id="form-line-guardian-phone" class="form-line{{ $errors->has('guardian_phone') ? ' focused error' : '' }}">
	                                	<input type="text" name="guardian_phone" class="form-control" id="guardian_phone" value="{{ old('guardian_phone-full') }}">
										<input id="guardian_phone-full" type="hidden" name="guardian_phone-full">
	                                </div>
	                                <label id="guardian-phone-error" class="error">{{ $errors->has('guardian_phone') ? $errors->first('guardian_phone') : '' }}</label>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="row clearfix">
	                        <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-4 col-xs-5 form-control-label">
	                            <label for="guardian_photo">Photo</label>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
	                            <img id="guardian_photo_preview" class="img-responsive img-circle" src="{{ old('guardian_photo_data') ? old('guardian_photo_data') : url('img/default-user.png') }}" width="50px">
								<input class="hide" type="file" accept="image/*" name="guardian_photo" id="guardian_photo">
								<input type="hidden" name="guardian_photo_data" id="guardian_photo_data" value="{{ old('guardian_photo_data') }}">
								<br>

								<a id="guardian_change_photo" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Select photo">
	                                <i class="material-icons">image</i>
	                            </a>

	                            <a id="guardian_reset_photo" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Reset">
	                                <i class="material-icons">refresh</i>
	                            </a>
	                        </div>
	                    </div>
                    </div> <!-- New -->

                    <input type="hidden" name="photo_change" id="photo_change">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-4 col-sm-6 col-xs-7">
                            <button type="submit" class="btn btn-primary waves-effect">SAVE</button>
                            <a href="{{ URL::previous() }}" class="btn btn-default waves-effect">CANCEL</a>
                        </div>
                    </div>
		        </div><!-- Body -->
	        </form>
		</div>
	</div>
</div>


<div class="modal fade" id="crop_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Crop the image</h4>
            </div>
            <div class="modal-body">
                <div>
					<img id="image_modal" src="{{ url('img/photo3.jpg') }}" alt="Picture" width="400px" height="400px">
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="crop_photo" class="btn btn-link waves-effect">SELECT</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript" src="{{ url('plugins/cropperjs/cropper.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/momentjs/moment.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/intl-tel-input/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/intl-tel-input/js/utils.js') }}"></script>
<script>
$(function() {
	var class_array =<?php echo json_encode($classes ); ?>;

	$("#phone").intlTelInput({
		preferredCountries: ['bd'],
		separateDialCode: true
    });

    $('#phone').focus(function() {
    	$(this).closest('.form-line').addClass('focused');
    })

	$('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        maxDate : new Date(),
        clearButton: true,
        weekStart: 1,
        time: false
    });

    var cropper = $('#image_modal');
	var cropBoxData;
	var canvasData;
	$("#form-student").submit(function() {
		$("#phone-full").val($("#phone").intlTelInput("getNumber"));
		$("#guardian_phone-full").val($("#guardian_phone").intlTelInput("getNumber"));

		if ($("#phone").val() != '' && !$("#phone").intlTelInput("isValidNumber")){
			$('#form-line-phone').addClass('focused error');
			$('#phone-error').html('Invalid phone number.');
			return false;
		}

		var guardian_type = $('.guardian_type:checked').val();

		if (guardian_type == "new") {
			if ($("#guardian_phone").val() != '' && !$("#guardian_phone").intlTelInput("isValidNumber")){
				$('#form-line-guardian-phone').addClass('focused error');
				$('#guardian-phone-error').html('Invalid phone number.');
				return false;
			}
		}
	});

	$('#crop_modal').on('shown.bs.modal', function () {
		cropper.cropper({
			autoCropArea: 0.5,
			aspectRatio: 1 / 1,
			dashed: false,
			viewMode: 1,
			dragMode: 'move',
			ready: function () {
				cropper.cropper('setCanvasData', canvasData);
				cropper.cropper('setCropBoxData', cropBoxData);
			}
		});
	}).on('hidden.bs.modal', function () {
		cropBoxData = cropper.cropper('getCropBoxData');
		canvasData = cropper.cropper('getCanvasData');
		cropper.cropper('destroy');
	});

	$('#change_photo').click(function() {
		$('#photo').click();
	});

	$( "#photo" ).change(function() {
		var _URL = window.URL || window.webkitURL;
		img = new Image();
		img.onerror = function() { 
			alert('Please chose an image file!'); 
		};

		img.onload = function () {
			$('#image_modal').attr('src',this.src);
			$("#crop_photo").attr('data-for', 'student');
			$('#crop_modal').modal('show');
		};
		img.src = _URL.createObjectURL(this.files[0]);
	});

	$('#crop_photo').click(function() {
		var cropped_image_data = cropper.cropper('getCroppedCanvas').toDataURL();
		$('#crop_modal').modal('hide');
		var photo_for = $(this).attr('data-for');

		if (photo_for == 'student'){
			$("#photo_preview").attr('src', cropped_image_data);
			$('#photo').val('');
			$('#photo_data').val(cropped_image_data);
		} else {
			$("#guardian_photo_preview").attr('src', cropped_image_data);
			$('#guardian_photo').val('');
			$('#guardian_photo_data').val(cropped_image_data);
		}
	});

	$('#reset_photo').click(function() {
		$("#photo_preview").attr("src", "{{ url('img/default-user.png') }}");
		$('#photo_data').val('');
        $('#photo_change').val('1');
	});
	$('#class_id').change(function() {
		var index = $(this).prop('selectedIndex');
		$('#section_id').html('');
		
		if (index != 0){
			var sections = class_array[index-1].sections;			
			var old_section_id = '{{ $student->section_id }}';
			$.each(sections, function( index, value ) {
				if (old_section_id == value.id)
					$('#section_id').append('<option value="' + value.id + '" selected>' + value.name + '</option>');
				else
			  		$('#section_id').append('<option value="' + value.id + '">' + value.name + '</option>');
			});

			$('#section_id').selectpicker('refresh');


		}
	});

	$('#class_id').trigger('change');

	//Guardian JS
	$("#guardian_phone").intlTelInput({
		preferredCountries: ['bd'],
		separateDialCode: true,
    });

    $('#guardian_phone').focus(function() {
    	$(this).closest('.form-line').addClass('focused');
    })

	$('.guardian_type').change(function() {
		var value = $(".guardian_type:checked").val();
		
		if (value == 'existing') {
			$('#existing_guardian').removeClass('hide');
			$('#new_guardian').addClass('hide');
		} else {
			$('#existing_guardian').addClass('hide');
			$('#new_guardian').removeClass('hide');
		}
	});

	$('#guardian_change_photo').click(function() {
		$('#guardian_photo').click();
	});

	$( "#guardian_photo" ).change(function() {
		var _URL = window.URL || window.webkitURL;
		img = new Image();
		img.onerror = function() { 
			alert('Please chose an image file!'); 
		};

		img.onload = function () {
			$('#image_modal').attr('src',this.src);
			$("#crop_photo").attr('data-for', 'guardian');
			$('#crop_modal').modal('show');
		};
		img.src = _URL.createObjectURL(this.files[0]);
	});

	$('#guardian_reset_photo').click(function() {
		$("#guardian_photo_preview").attr("src", "{{ url('img/default-user.png') }}");
		$('#guardian_photo_data').val('');
	});

	$('.guardian_type').trigger('change');
});
</script>
@stop