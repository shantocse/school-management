@extends('layouts.app')

@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
		    <div class="body bg-white clearfix">
				<div class="pull-right">
					<a id="btn-add-year" class="btn btn-primary btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" data-original-title="Add Academic Year">
			            <i class="material-icons">add</i>
			    	</a>
				</div>
		    </div>
		</div>
	</div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    LIST OF ACADEMIC YEARS
                </h2>
            </div>

            <div class="body table-responsive">
				<table class="table table-hover" id="table-years">
					<thead>
						<tr>
							<th>Name</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						@foreach($years as $year)
							<tr>
								<td>{{ $year->name }} {{ $year->default == 1 ? ' (Default)' : '' }}</td>
								<td class="text-right">
	                            	<div class="btn-group">
	                                    <button type="button" class="btn bg-cyan dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	                                         <i class="material-icons">more_vert</i>
	                                    </button>
	                                    <ul class="dropdown-menu dropdown-menu-right">
	                                    	@if (!$year->default == 1)
												<li><a class="make-default waves-effect waves-block" data-id="{{ $year->id }}"><i class="material-icons">adjust</i> Make Default</a></li>
												<li role="separator" class="divider"></li>
											@endif

	                                        <li><a class=" waves-effect waves-block edit" data-name="{{ $year->name }}" data-id="{{ $year->id }}" data-default="{{ $year->default }}"><i class="material-icons">edit</i> Edit</a></li>

	                                        <li><a class=" waves-effect waves-block delete" data-id="{{ $year->id }}"><i class="material-icons">delete</i> Delete</a></li>
	                                    </ul>
	                                </div>
	                            </td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
      	</div>
	</div>
</div>


<div class="modal fade" role="dialog" id="add-year-modal">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Add Academic Year</h4>
			</div>
			<div class="modal-body">
				<form>
					<label for="name" class="control-label">Name:</label>
					<div class="form-group">
                        <div class="form-line" id="form-line-name">
                            <input type="text" class="form-control" id="name" placeholder="Enter academic year">
                        </div>
                        <label class="error" id="name-error"></label>
                    </div>

					<input type="checkbox" id="default-year" class="filled-in chk-col-blue">
					<label for="default-year">Make default year</label>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btn-add-modal" class="btn btn-link waves-effect">ADD</button>
				<button type="button" id="btn-save-modal" class="btn btn-link waves-effect hide">SAVE</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-red">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Delete Academic Year</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <button id="btn-delete" type="button" class="btn btn-link waves-effect">DELETE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript">
	$(function() {
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		$('#btn-add-year').click(function() {
			$('#add-year-modal').modal('show');
		});

		$('#btn-add-modal').click(function() {
			if (!$('#name').val()) {
				$('#form-line-name').addClass('focused error');
				$('#name-error').html('Enter academic year.');
			} else {
				var name = $('#name').val();

				if ($('#default-year').is(':checked'))
					var default_year = 1;
				else
					var default_year = 0;

				$('#add-year-modal').modal('hide');

				$.ajax({
					method: "POST",
					url: "{{ route('add_year') }}",
					data: { name: name, default: default_year}
				}).done(function( msg ) {
					location.reload();
				});
			}
		});

		$('#add-year-modal').on('hide.bs.modal', function (e) {
			$('#form-line-name').removeClass('focused');
			$('#form-line-name').removeClass('error');
			$('#name-error').html('');
			$('#name').val('');
			$('#default-year').prop('checked', false);

			$('#btn-save-modal').addClass('hide');
			$('#btn-add-modal').removeClass('hide');
		});

		$('.make-default').click(function() {
			var id = $(this).data('id');

			$.ajax({
				method: "POST",
				url: "{{ route('make_default_year') }}",
				data: { id: id}
			}).done(function( msg ) {
				location.reload();
			});
		});

		$('.delete').click(function() {
			var id = $(this).data('id');
			var index = $(this).closest('tr').index('tr');
			$('#btn-delete').attr("data-id", id);
			$('#btn-delete').attr("data-index", index);
			$('#delete-modal').modal('show');
		});

		$('#btn-delete').click(function() {
			var id = $(this).data('id');
			var index = $(this).data('index');

			$.ajax({
				method: "POST",
				url: "{{ route('delete_year') }}",
				data: { id: id}
			}).done(function( msg ) {
				location.reload();
			});
		});

		$('.edit').click(function() {
			var name = $(this).data('name');
			var id = $(this).data('id');
			var default_year = $(this).data('default');

			$('#btn-save-modal').attr("data-id", id);
			$('#btn-save-modal').removeClass('hide');
			$('#btn-add-modal').addClass('hide');

			$('#name').val(name);
			if (default_year == 1)
				$('#default-year').prop('checked', true);

			$('#add-year-modal').modal('show');
		});

		$('#btn-save-modal').click(function() {
			var id = $(this).data('id');

			if (!$('#name').val()) {
				$('#form-line-name').addClass('focused error');
				$('#name-error').html('Enter academic year.');
			} else {
				var name = $('#name').val();

				if ($('#default-year').is(':checked'))
					var default_year = 1;
				else
					var default_year = 0;

				$('#add-year-modal').modal('hide');

				$.ajax({
					method: "POST",
					url: "{{ route('edit_year') }}",
					data: { id: id, name: name, default: default_year}
				}).done(function( msg ) {
					location.reload();
				});
			}
		});
	});
</script>
@stop